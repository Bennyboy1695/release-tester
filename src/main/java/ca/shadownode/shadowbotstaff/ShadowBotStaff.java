package ca.shadownode.shadowbotstaff;

import ca.shadownode.shadowbotstaff.command.*;
import ca.shadownode.shadowbotstaff.command.punishment.PunishmentCommand;
import ca.shadownode.shadowbotstaff.command.punishment.PunishmentHistoryCommand;
import ca.shadownode.shadowbotstaff.listener.PunishmentEditListener;
import ca.shadownode.shadowbotstaff.logging.Logger;
import ca.shadownode.shadowbotstaff.storage.SqlManager;
import ca.shadownode.shadowbotstaff.storage.oldMongo.MongoController;
import ca.shadownode.shadowbotstaff.storage.oldMongo.MongoRequestRegistry;
import ca.shadownode.shadowbotstaff.utilities.Punishment;
import ca.shadownode.shadowbotstaff.utilities.Server;
import ca.shadownode.shadowbotstaff.utilities.Util;
import com.google.gson.JsonObject;
import me.bhop.bjdautilities.Messenger;
import me.bhop.bjdautilities.ReactionMenu;
import me.bhop.bjdautilities.command.CommandHandler;
import me.bhop.bjdautilities.command.handler.GuildIndependentCommandHandler;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.hooks.InterfacedEventManager;
import net.dv8tion.jda.core.utils.tuple.Pair;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ShadowBotStaff {

    private final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(2);
    private JDA jda;
    private Messenger messenger;
    private ArrayList<Server> availableServers;

    private Path directory;
    private Config mainConfig;
    private JsonObject mainConf;
    private CommandHandler commandHandler;
    private MongoController mongoController;
    private MongoRequestRegistry mongoRequestRegistry;
    private SqlManager sqlManager;
    private SqlManager.PunishPluginSqlManager punishPluginSqlManager;

    public void init(Path directory) throws Exception {
        this.directory = directory;

        Logger.info("Initializing Config!");
        try {
            initConfig(directory);
        } catch (Exception e) {
            throw new RuntimeException("Failed to initialize config!", e);
        }

        if (mainConfig.getConfigValue("token").getAsString().equalsIgnoreCase("change_me")) {
            Logger.error("Found unedited config. Please add the token.");
            System.exit(1);
        }

        //mongoController = new MongoController(getMainConfig().getConfigValue("mongo_settings", "hostname").getAsString(),
        //        getMainConfig().getConfigValue("mongo_settings", "port").getAsInt(),
        //        getMainConfig().getConfigValue("mongo_settings", "database").getAsString(),
        //        getMainConfig().getConfigValue("mongo_settings", "username").getAsString(),
        //        getMainConfig().getConfigValue("mongo_settings", "password").getAsString(),
        //        getMainConfig().getConfigValue("mongo_settings", "authDb").getAsString());
//
        //mongoRequestRegistry = new MongoRequestRegistry(this);

        try {
            Logger.info("Initialising SQL stuff...");
            sqlManager = new SqlManager(getMainConfig().getConfigValue("sql_settings", "hostname").getAsString(),
                    getMainConfig().getConfigValue("sql_settings", "port").getAsInt(),
                    getMainConfig().getConfigValue("sql_settings", "database").getAsString(),
                    getMainConfig().getConfigValue("sql_settings", "username").getAsString(),
                    getMainConfig().getConfigValue("sql_settings", "password").getAsString());

            punishPluginSqlManager = new SqlManager.PunishPluginSqlManager(this, getMainConfig().getConfigValue("hammerSqlSettings", "hostname").getAsString(),
                    getMainConfig().getConfigValue("hammerSqlSettings", "port").getAsInt(),
                    getMainConfig().getConfigValue("hammerSqlSettings", "database").getAsString(),
                    getMainConfig().getConfigValue("hammerSqlSettings", "username").getAsString(),
                    getMainConfig().getConfigValue("hammerSqlSettings", "password").getAsString());
            Logger.info("Initialised SQL Stuff!");
        } catch (Exception e) {
            Logger.error("Failed to initialise SQL stuff!");
        }

        this.jda = new JDABuilder(AccountType.BOT)
                .setToken(mainConfig.getConfigValue("token").getAsString())
                .setBulkDeleteSplittingEnabled(false)
                .setEventManager(new ThreadedEventManager())
                .build();
        jda.awaitReady();

        GuildIndependentCommandHandler.Builder builder = new CommandHandler.Builder(jda).setEntriesPerHelpPage(6).addCustomParameter(this).guildIndependent();
        builder.setCommandLifespan(10);
        builder.setPrefix(mainConfig.getConfigValue("prefix").getAsString());
        builder.setResponseLifespan(20);
        commandHandler = builder.build();

        Logger.info("Loading Messenger...");
        this.messenger = new Messenger();

        Logger.info("Registering Commands...");
        commandHandler.register(new ActionCommand.Restart());
        commandHandler.register(new ActionCommand.Stop());
        commandHandler.register(new ActionCommand.Start());
        commandHandler.register(new LockdownCommand());
        commandHandler.register(new ServersCommand());
        commandHandler.register(new RefreshServersCommand());
        commandHandler.register(new PunishmentCommand());
        commandHandler.register(new PunishmentHistoryCommand());
        commandHandler.register(new TestCommand());

        jda.addEventListener(new PunishmentEditListener(this));

        availableServers = Util.getAllServers(this);

        executorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    if (getMainConfig().getConfigValue("hammerSqlSettings", "checkBans").getAsBoolean())
                        makeBanEmbeds();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (getMainConfig().getConfigValue("hammerSqlSettings", "checkMutes").getAsBoolean())
                        makeMuteEmbeds();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 0, getMainConfig().getConfigValue("punishment_update_check").getAsLong(), TimeUnit.SECONDS);
    }

    public void shutdown() {
        Logger.info("Initiating Shutdown...");
        jda.shutdown();
        Logger.info("Shutdown Complete.");
    }

    private void initConfig(Path configDirectory) {
        try {
            mainConfig = new Config(this, configDirectory);
            mainConf = mainConfig.newConfig("config", Config.mainConfigDefaults());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JDA getJda() {
        return jda;
    }

    public Messenger getMessenger() {
        return messenger;
    }

    public SqlManager getSqlManager() {
        return sqlManager;
    }

    public SqlManager.PunishPluginSqlManager getPunishPluginSqlManager() {
        return punishPluginSqlManager;
    }

    public MongoController getMongoController() {
        return mongoController;
    }

    public MongoRequestRegistry getMongoRequestRegistry() {
        return mongoRequestRegistry;
    }

    public CommandHandler getCommandHandler() {
        return commandHandler;
    }

    public Config getMainConfig() {
        return mainConfig;
    }

    public JsonObject getMainConf() {
        return mainConf;
    }

    public Path getDirectory() {
        return directory;
    }

    public boolean getLockdownStatus() {
        return getMainConfig().getConfigValue("NO_TOUCH").getAsJsonObject().get("inlockdown").getAsBoolean();
    }

    public void setAvailableServers(ArrayList<Server> availableServers) {
        this.availableServers = availableServers;
    }

    public ArrayList<Server> getAvailableServers() {
        return availableServers;
    }

    public void reloadConfig() {
        try {
            this.initConfig(getDirectory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void makeBanEmbeds() {
        ArrayList<Punishment> bans = getPunishPluginSqlManager().getBans();
        TextChannel channel = getJda().getTextChannelById(getMainConfig().getConfigValue("punishmentChannelId").getAsLong());
        Logger.info("Checking for new bans!");
        Logger.info("Found " + bans.size() + " new bans!");
        if (!bans.isEmpty()) {
            for (Punishment punishment : bans) {
                Pair<UUID, String> pair = Util.getPlayerInfoFromUUID(getPunishPluginSqlManager().getPlayerUUIDFromId(punishment.getPlayer()));
                long banLength = punishment.getPunishedUntil() - punishment.getPunishedOn();
                long minutes = TimeUnit.MILLISECONDS.toMinutes(banLength);
                EmbedBuilder punishmentEmbedBuilder = new EmbedBuilder().setTitle("**" + pair.getRight().toString().replace("_", "\\_") + "** Punishment Record")
                        .setColor(Color.decode("#ee82ee"))
                        .setDescription("**UUID:** " + ("[" + pair.getLeft() + "](https://namemc.com/search?q=" + pair.getLeft() + ")"))
                        .appendDescription("\n" + "**Banned By:** " + (punishment.getStaff() == 1 ? "Console" : jda.getGuildById(channel.getGuild().getIdLong()).getMembersByEffectiveName(Util.getPlayerInfoFromUUID(getPunishPluginSqlManager().getPlayerUUIDFromId(punishment.getStaff())).getRight(), true).get(0).getAsMention()))
                        .appendDescription("\n" + "**Server:** " + (punishment.getServer() == 0 ? "Global" : Util.getClosestMatchServer(this, getPunishPluginSqlManager().getServerNameFromId(punishment.getServer()))))
                        .appendDescription("\n" + "**Punishment:** " + (punishment.isPerm() ? "Permanent Ban" : Util.convertToDaysHoursMinutes(minutes) + " Ban"))
                        .appendDescription("\n" + "**Reason:** " + punishment.getReason())
                        .setThumbnail("https://mc-heads.net/avatar/" + pair.getLeft().toString() + ".png");
                executorService.schedule(() -> {
                    new ReactionMenu.Builder(getJda())
                            .setEmbed(punishmentEmbedBuilder.build())
                            .setStartingReactions("\uD83D\uDCDD")
                            .buildAndDisplay(channel);
                }, 5, TimeUnit.SECONDS);
            }
        }
    }

    public void makeMuteEmbeds() {
        ArrayList<Punishment> mutes = getPunishPluginSqlManager().getMutes();
        TextChannel channel = getJda().getTextChannelById(getMainConfig().getConfigValue("punishmentChannelId").getAsLong());
        Logger.info("Checking for new mutes!");
        Logger.info("Found " + mutes.size() + " new mutes!");
        if (!mutes.isEmpty()) {
            for (Punishment punishment : mutes) {
                Pair<UUID, String> pair = Util.getPlayerInfoFromUUID(getPunishPluginSqlManager().getPlayerUUIDFromId(punishment.getPlayer()));
                long muteLength = punishment.getPunishedUntil() - punishment.getPunishedOn();
                long minutes = TimeUnit.MILLISECONDS.toMinutes(muteLength);
                EmbedBuilder punishmentEmbedBuilder = new EmbedBuilder().setTitle("**" + pair.getRight().toString().replace("_", "\\_") + "** Punishment Record")
                        .setColor(Color.decode("#ee82ee"))
                        .setDescription("**UUID:** " + ("[" + pair.getLeft() + "](https://namemc.com/search?q=" + pair.getLeft() + ")"))
                        .appendDescription("\n" + "**Muted By:** " + (punishment.getStaff() == 1 ? "Console" : jda.getGuildById(channel.getGuild().getIdLong()).getMembersByEffectiveName(Util.getPlayerInfoFromUUID(getPunishPluginSqlManager().getPlayerUUIDFromId(punishment.getStaff())).getRight(), true).get(0).getAsMention()))
                        .appendDescription("\n" + "**Server:** " + (punishment.getServer() == 0 ? "Global" : Util.getClosestMatchServer(this, getPunishPluginSqlManager().getServerNameFromId(punishment.getServer()))))
                        .appendDescription("\n" + "**Punishment:** " + (punishment.isPerm() ? "Permanent Mute" : Util.convertToDaysHoursMinutes(minutes) + " Mute"))
                        .appendDescription("\n" + "**Reason:** " + punishment.getReason())
                        .setThumbnail("https://mc-heads.net/avatar/" + pair.getLeft().toString() + ".png");
                executorService.schedule(() -> {
                    new ReactionMenu.Builder(getJda())
                            .setEmbed(punishmentEmbedBuilder.build())
                            .setStartingReactions("\uD83D\uDCDD")
                            .buildAndDisplay(channel);
                }, 5, TimeUnit.SECONDS);
            }
        }
    }

    private final class ThreadedEventManager extends InterfacedEventManager {
        private final ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);

        @Override
        public void handle(Event e) {
            executor.submit(() -> super.handle(e));
        }
    }
}
