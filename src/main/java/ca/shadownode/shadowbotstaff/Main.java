package ca.shadownode.shadowbotstaff;

import ca.shadownode.shadowbotstaff.logging.Logger;
import com.google.gson.JsonObject;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) throws Exception {
        final ExecutorService console = Executors.newSingleThreadExecutor();
        final ShadowBotStaff bot = new ShadowBotStaff();
        Path mainDirectory = Paths.get(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParent();

        console.submit(() -> {
            final Scanner input = new Scanner(System.in);

            String cmd;
            do {
                cmd = input.nextLine();
                switch (cmd.toLowerCase()) {
                    case "lockdown":
                        JsonObject config = bot.getMainConf();
                        JsonObject object = config.get("NO_TOUCH").getAsJsonObject();
                        if (object.get("inlockdown").getAsBoolean()) {
                            object.addProperty("inlockdown", false);
                            Logger.warn("Lockdown has been disabled!");
                        } else if (!object.get("inlockdown").getAsBoolean()) {
                            object.addProperty("inlockdown", true);
                            Logger.warn("Lockdown has been enabled!");
                        }
                        bot.getMainConfig().saveConfig("config", config);
                        bot.reloadConfig();
                        break;
                    case "exit":
                        break;
                    default:
                        System.out.println("Invalid Command.");
                }
            } while (!cmd.equalsIgnoreCase("exit"));

            bot.shutdown();
            System.exit(0);
        });
        if (!mainDirectory.toFile().exists())
            mainDirectory.toFile().mkdir();
        bot.init(mainDirectory);
    }

}
