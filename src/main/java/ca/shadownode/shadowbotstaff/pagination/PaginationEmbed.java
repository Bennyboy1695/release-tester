package ca.shadownode.shadowbotstaff.pagination;

import me.bhop.bjdautilities.ReactionMenu;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.TextChannel;

import java.util.LinkedList;
import java.util.List;

public class PaginationEmbed {

    private final JDA jda;
    private ReactionMenu reactionMenu = null;
    private final String prevEmote, forwardEmote, removeEmote;
    private final List<Page> pages;
    private int currentPage = 0;

    public PaginationEmbed(JDA jda, String prevEmote, String forwardEmote, String removeEmote, List<Page> pages) {
        this.jda = jda;
        this.prevEmote = prevEmote;
        this.forwardEmote = forwardEmote;
        this.removeEmote = removeEmote;
        this.pages = pages;
        currentPage = 0;
        if (pages.isEmpty())
            throw new IllegalStateException("You cannot have an empty PaginationEmbed!");
        ReactionMenu.Builder reactionBuilder = new ReactionMenu.Builder(jda);
        reactionBuilder.setEmbed(pages.get(currentPage).getGeneratedPage());
        reactionBuilder.onClick(removeEmote, ReactionMenu::destroy);
        if (pages.size() > 1) {
            //Prev
            reactionBuilder.onClick(prevEmote, (prev, user) -> {
                if (currentPage == 0)
                    return;
                currentPage--;
                prev.getMessage().setContent(pages.get(currentPage).getGeneratedPage());
            });
            //Forward
            reactionBuilder.onClick(forwardEmote, (forward, user) -> {
                if (currentPage == pages.size())
                    return;
                currentPage++;
                forward.getMessage().setContent(pages.get(currentPage).getGeneratedPage());
            });
        }
        reactionMenu = reactionBuilder.build();
    }

    public void display(TextChannel channel) {
        if (reactionMenu.getMessage() != null)
            throw new IllegalStateException("This embed has already been displayed!");
        reactionMenu.display(channel);
    }

    public void delete() {
        deleteIn(0);
    }

    public void deleteIn(int seconds) {
        if (reactionMenu.getMessage() == null)
            return;
        reactionMenu.destroyIn(seconds);
    }

    public ReactionMenu getReactionMenu() {
        return reactionMenu;
    }

    public static class Builder {

        private final JDA jda;
        private String prevEmote = "\u25C0", forwardEmote = "\u25B6", removeEmote = "\u274C";
        private final List<Page> pages = new LinkedList<>();

        public Builder(JDA jda) {
            this.jda = jda;
        }

        public Builder addPage(Page page) {
            this.pages.add(page);
            return this;
        }

        public Builder setPrevEmote(String prevEmote) {
            this.prevEmote = prevEmote;
            return this;
        }

        public Builder setForwardEmote(String forwardEmote) {
            this.forwardEmote = forwardEmote;
            return this;
        }

        public Builder setRemoveEmote(String removeEmote) {
            this.removeEmote = removeEmote;
            return this;
        }

        public PaginationEmbed build() {
            return new PaginationEmbed(jda, prevEmote, forwardEmote, removeEmote, pages);
        }

        public PaginationEmbed buildAndDisplay(TextChannel channel) {
            PaginationEmbed paginationEmbed = build();
            paginationEmbed.display(channel);
            return paginationEmbed;
        }
    }
}
