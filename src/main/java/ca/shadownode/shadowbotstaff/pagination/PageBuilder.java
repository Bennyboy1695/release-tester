package ca.shadownode.shadowbotstaff.pagination;

import java.awt.Color;
import java.util.LinkedList;
import java.util.List;

public class PageBuilder {

    private String title, titleUrl, footer, avatarUrl;
    private CharSequence description;
    private Color color;
    private boolean includeTimestamp;
    private int entryLimit = 6;
    private final List<Page.Entry> contents = new LinkedList<>();

    public PageBuilder() {}

    public PageBuilder setTitle(String title) {
        return setTitle(title, null);
    }

    public PageBuilder setTitle(String title, String titleUrl) {
        this.title = title;
        this.titleUrl = titleUrl;
        return this;
    }

    public PageBuilder setDescription(CharSequence description) {
        this.description = description;
        return this;
    }

    public PageBuilder setColor(Color color) {
        this.color = color;
        return this;
    }

    public PageBuilder setFooter(String footer) {
        return setFooter(footer, null);
    }

    public PageBuilder setFooter(String footer, String avatarUrl) {
        this.footer = footer;
        this.avatarUrl = avatarUrl;
        return this;
    }

    public PageBuilder includeTimestamp(boolean includeTimestamp) {
        this.includeTimestamp = includeTimestamp;
        return this;
    }

    public PageBuilder setEntryLimit(int entryLimit) {
        this.entryLimit = entryLimit;
        return this;
    }

    public PageBuilder addContent(Page.Entry content) {
        return addContent(content.isInline(), content.getTitle(), content.getLines());
    }

    public PageBuilder addContent(boolean inline, String title, String... lines) {
        if (this.contents.size() > entryLimit) {
            throw new IllegalStateException("Cannot add more then the entry limits allow!");
        } else {
            this.contents.add(new Page.Entry(inline, title, lines));
        }
        return this;
    }

    public Page build() {
       return new Page(title, titleUrl, description, color, footer, avatarUrl, includeTimestamp, entryLimit, contents);
    }

}
