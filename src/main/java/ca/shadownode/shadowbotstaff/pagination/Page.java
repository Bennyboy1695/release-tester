package ca.shadownode.shadowbotstaff.pagination;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.MessageEmbed;

import java.awt.*;
import java.time.Instant;
import java.util.List;

public class Page {

    private final String title, titleUrl, footer, avatarUrl;
    private final CharSequence description;
    private final Color color;
    private final boolean includeTimestamp;
    private final List<Entry> contents;
    private final int entryLimit;

    protected Page(String title, String titleUrl, CharSequence description, Color color, String footer, String avatarUrl, boolean includeTimestamp, int entryLimit, List<Entry> contents) {
        this.title = title;
        this.titleUrl = titleUrl;
        this.description = description;
        this.color = color;
        this.footer = footer;
        this.avatarUrl = avatarUrl;
        this.includeTimestamp = includeTimestamp;
        this.contents = contents;
        this.entryLimit = entryLimit;
    }

    public String getTitle() {
        return title;
    }

    public String getTitleUrl() {
        return titleUrl;
    }

    public Color getColor() {
        return color;
    }

    public String getFooter() {
        return footer;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public boolean includeTimestamp() {
        return includeTimestamp;
    }

    public CharSequence getDescription() {
        return description;
    }

    public int getEntryLimit() {
        return entryLimit;
    }

    public List<Entry> getContents() {
        return contents;
    }

    protected MessageEmbed getGeneratedPage() {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle(getTitle(), getTitleUrl());
        embedBuilder.setDescription(getDescription());
        embedBuilder.setColor(getColor());
        embedBuilder.setFooter(getFooter(), getAvatarUrl());
        if (includeTimestamp()) {
            embedBuilder.setTimestamp(Instant.now());
        }
        getContents().forEach(entry -> {
            StringBuilder builder = new StringBuilder();
            for (String str : entry.getLines()) {
                builder.append(str).append("\n");
            }
            embedBuilder.addField(entry.getTitle(), builder.toString(), entry.isInline());
        });
        return embedBuilder.build();
    }

    public static class Entry {

        protected final String title;
        protected final String[] lines;
        protected final boolean inline;

        public Entry(boolean inline, String title, String... lines ) {
            this.title = title;
            this.lines = lines;
            this.inline = inline;
        }

        public String getTitle() {
            return title;
        }

        public String[] getLines() {
            return lines;
        }

        public boolean isInline() {
            return inline;
        }
    }
}
