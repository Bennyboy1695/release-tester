package ca.shadownode.shadowbotstaff.command;

import ca.shadownode.shadowbotstaff.ShadowBotStaff;
import ca.shadownode.shadowbotstaff.utilities.Util;
import me.bhop.bjdautilities.command.annotation.Command;
import me.bhop.bjdautilities.command.annotation.Execute;
import me.bhop.bjdautilities.command.result.CommandResult;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

import java.awt.*;
import java.util.List;

@Command(label = "refresh", description = "When used it will refresh the list of available servers. Useful to update the server list when a new server has been added so that you don't have to restart the bot", usage = "refresh")
public class RefreshServersCommand {

    @Execute
    public CommandResult onRefresh(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowBotStaff bot) {
        bot.setAvailableServers(Util.getAllServers(bot));
        bot.getMessenger().sendEmbed(channel, new EmbedBuilder().setColor(Color.GREEN).setTitle("**Success**!").setDescription("Successfully refreshed the available servers!").build(), 10);
        return CommandResult.success();
    }
}
