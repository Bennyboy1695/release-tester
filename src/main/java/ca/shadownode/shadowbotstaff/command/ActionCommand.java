package ca.shadownode.shadowbotstaff.command;

import ca.shadownode.shadowbotstaff.logging.Logger;
import ca.shadownode.shadowbotstaff.ShadowBotStaff;
import ca.shadownode.shadowbotstaff.utilities.Server;
import ca.shadownode.shadowbotstaff.utilities.Util;
import com.google.gson.JsonObject;
import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;
import me.bhop.bjdautilities.ReactionMenu;
import me.bhop.bjdautilities.command.annotation.Command;
import me.bhop.bjdautilities.command.annotation.Execute;
import me.bhop.bjdautilities.command.result.CommandResult;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;

import java.awt.*;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class ActionCommand {

    private static final ScheduledExecutorService scheduledTask = Executors.newScheduledThreadPool(2);

    private static boolean doAction(ShadowBotStaff bot, String action, String server) {
        try {
            URL url = new URL(bot.getMainConfig().getConfigValue("apilink").getAsString() + "/api/client/servers/" + server + "/power");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Bearer " + bot.getMainConfig().getConfigValue("apikey").getAsString());
            connection.setRequestProperty("Accept", "Application/vnd.pterodactyl.v1+json");
            connection.setDoOutput(true);
            JsonObject object = new JsonObject();
            object.addProperty("signal", action.toLowerCase());
            connection.connect();
            try (OutputStream os = connection.getOutputStream()) {
                os.write(object.toString().getBytes(StandardCharsets.UTF_8));
            }
            if (connection.getResponseCode() == 204) {
                return true;
            } else {
                Logger.warn("Request failed with response code: " + connection.getResponseCode());
                return false;
            }
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Command(label = "restart", minArgs = 1, description = "When provided with a server it will send a request to that server for it to restart itself!", usage = "restart <server>")
    public static class Restart {

        @Execute
        public CommandResult onRestart(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowBotStaff bot) {
            if (!args.isEmpty()) {
                if (!bot.getLockdownStatus()) {
                    if (member.getRoles().stream().map(Role::getIdLong).anyMatch((role) -> role == bot.getMainConfig().getConfigValue("statusRoleId").getAsLong())) {
                        String serverArg = String.join(" ", args);
                        ArrayList<Server> matchedServers = new ArrayList<>();
                        int lowest = 100;
                        for (Server server : bot.getAvailableServers()) {
                            int maybe = Util.computeLevenshteinDistance(server.getName(), serverArg);
                            if (maybe < lowest) {
                                lowest = maybe;
                                matchedServers = new ArrayList<>();
                                matchedServers.add(server);
                            } else if (maybe == lowest) {
                                matchedServers.add(server);
                            }
                        }
                        if (matchedServers.size() == 1) {
                            AtomicBoolean sendRestart = new AtomicBoolean(true);
                            Server server = matchedServers.get(0);
                            final ReactionMenu[] menu = {null};
                            ReactionMenu.Builder builder = new ReactionMenu.Builder(bot.getJda());
                            builder.setEmbed(new EmbedBuilder().setTitle("Restarting...").setColor(Color.ORANGE).setDescription("Sending a restart request to `" + server.getName() + "` as requested by " + member.getAsMention()).setFooter("React with a \uD83D\uDED1 within 10 seconds to stop the restart request!", null).build())
                                    .onClick("\uD83D\uDED1", (click, user) -> {
                                        sendRestart.set(false);
                                        menu[0].removeReaction("\uD83D\uDED1");
                                        menu[0].getMessage().setContent(new EmbedBuilder().setColor(Color.RED).setTitle("Restart Cancelled!").setDescription("The restart for `" + server.getName() + "` failed to send because it was cancelled by " + user.getAsMention() + "!").build());
                                    });
                            builder.onDisplay(display -> {
                                scheduledTask.schedule(() -> {
                                    if (sendRestart.get()) {
                                        if (ActionCommand.doAction(bot, "restart", server.getIdentifier())) {
                                            menu[0].removeReaction("\uD83D\uDED1");
                                            menu[0].getMessage().setContent(new EmbedBuilder().setTitle("Restarted!").setColor(Color.GREEN).setDescription("The restart was successfully sent to `" + server.getName() + "` as requested by " + member.getAsMention()).build());
                                        } else {
                                            menu[0].getMessage().setContent(new EmbedBuilder().setTitle("Restart Failed!").setColor(Color.RED).setDescription("The restart failed to send to `" + server.getName() + "` as requested by " + member.getAsMention()).build());
                                        }
                                    }
                                }, 10, TimeUnit.SECONDS);
                            });
                            menu[0] = builder.buildAndDisplay(channel);
                            menu[0].destroyIn(20);
                            return CommandResult.success();
                        } else if (matchedServers.size() > 1) {
                            final ReactionMenu[] builtMenu = {null};
                            ReactionMenu.Builder menuBuilder = new ReactionMenu.Builder(bot.getJda());
                            EmbedBuilder embed = new EmbedBuilder().setTitle("Multiple Matches Found!").setColor(Color.CYAN);
                            embed.setDescription("To pick the match you want, please react with the emoji appertaining to the server name!");
                            char endCharacter = 'a';
                            String alias = "regional_indicator_symbol_";
                            for (Server server : matchedServers) {
                                Emoji emoji = EmojiManager.getForAlias(alias + endCharacter);
                                embed.addField(server.getName() + ": ", emoji.getUnicode(), true);
                                endCharacter++;
                                menuBuilder.onClick("\u274C", ReactionMenu::destroy);
                                menuBuilder.onClick(emoji.getUnicode(), (react, user) -> {
                                    AtomicBoolean sendStop = new AtomicBoolean(true);
                                    final ReactionMenu[] menu = {null};
                                    ReactionMenu.Builder builder = new ReactionMenu.Builder(bot.getJda());
                                    builder.setEmbed(new EmbedBuilder().setTitle("Restarting...").setColor(Color.ORANGE).setDescription("Sending a restart request to `" + server.getName() + "` as requested by " + member.getAsMention()).setFooter("React with a \uD83D\uDED1 within 10 seconds to stop the restart request!", null).build())
                                            .onClick("\uD83D\uDED1", (click, clickedUser) -> {
                                                sendStop.set(false);
                                                menu[0].removeReaction("\uD83D\uDED1");
                                                menu[0].getMessage().setContent(new EmbedBuilder().setColor(Color.RED).setTitle("Restart Cancelled!").setDescription("The restart for `" + server.getName() + "` failed to send because it was cancelled by " + clickedUser.getAsMention() + "!").build());
                                            });
                                    builder.onDisplay(display -> {
                                        scheduledTask.schedule(() -> {
                                            if (sendStop.get()) {
                                                if (ActionCommand.doAction(bot, "restart", server.getIdentifier())) {
                                                    menu[0].removeReaction("\uD83D\uDED1");
                                                    menu[0].getMessage().setContent(new EmbedBuilder().setTitle("Restarted!").setColor(Color.GREEN).setDescription("The restart was successfully sent to `" + server.getName() + "` as requested by " + member.getAsMention()).build());
                                                } else {
                                                    menu[0].getMessage().setContent(new EmbedBuilder().setTitle("Restart Failed!").setColor(Color.RED).setDescription("The restart failed to send to `" + server.getName() + "` as requested by " + member.getAsMention()).build());
                                                }
                                            }
                                        }, 10, TimeUnit.SECONDS);
                                    });
                                    menu[0] = builder.buildAndDisplay(channel);
                                    menu[0].destroyIn(20);
                                    if (builtMenu[0] != null)
                                        builtMenu[0].destroy();
                                });
                            }
                            menuBuilder.setEmbed(embed.build());
                            builtMenu[0] = menuBuilder.buildAndDisplay(channel);
                            builtMenu[0].destroyIn(20);
                            return CommandResult.success();
                        } else {
                            bot.getMessenger().sendEmbed(channel, new EmbedBuilder().setColor(Color.RED).setTitle("Error").setDescription("We couldn't find a server with a name close enough to: `" + serverArg + "`").addField("Tip:", "Run `" + bot.getMainConfig().getConfigValue("prefix").getAsString() + "servers` to see a list of servers available for you to restart!", true).build(), 15);
                            return CommandResult.success();
                        }
                    } else {
                        Role role = bot.getJda().getRoleById(bot.getMainConfig().getConfigValue("statusRoleId").getAsLong());
                        bot.getMessenger().sendEmbed(channel, new EmbedBuilder().setTitle("Error").setDescription("You do not have the required role: `" + role.getName() + "` to use this command!").setColor(Color.RED).build(), 10);
                        return CommandResult.success();
                    }
                } else {
                    bot.getMessenger().sendEmbed(channel, new EmbedBuilder().setColor(Color.MAGENTA).setTitle("Restart failed!").setDescription("The restart failed to send because the bot is in lockdown. This was enabled because a member was potentially compromised and was abusing the bot!").setFooter("To disable lockdown a staff member will need to run 'lockdown' in the bots console!", null).build(), 10);
                    return CommandResult.success();
                }
            }
            return CommandResult.invalidArguments();
        }
    }

    @Command(label = "stop", description = "When provided with a server it will send a request to that server for it to stop itself!", usage = "stop <server>")
    public static class Stop {

        @Execute
        public CommandResult onStop(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowBotStaff bot) {
            if (!args.isEmpty()) {
                if (!bot.getLockdownStatus()) {
                    if (member.getRoles().stream().map(Role::getIdLong).anyMatch((role) -> role == bot.getMainConfig().getConfigValue("statusRoleId").getAsLong())) {
                        String serverArg = String.join(" ", args);
                        ArrayList<Server> matchedServers = new ArrayList<>();
                        int lowest = 100;
                        for (Server server : bot.getAvailableServers()) {
                            int maybe = Util.computeLevenshteinDistance(server.getName(), serverArg);
                            if (maybe < lowest) {
                                lowest = maybe;
                                matchedServers = new ArrayList<>();
                                matchedServers.add(server);
                            } else if (maybe == lowest) {
                                matchedServers.add(server);
                            }
                        }
                        if (matchedServers.size() == 1) {
                            AtomicBoolean sendStop = new AtomicBoolean(true);
                            Server server = matchedServers.get(0);
                            final ReactionMenu[] menu = {null};
                            ReactionMenu.Builder builder = new ReactionMenu.Builder(bot.getJda());
                            builder.setEmbed(new EmbedBuilder().setTitle("Stopping...").setColor(Color.ORANGE).setDescription("Sending a stop request to `" + server.getName() + "` as requested by " + member.getAsMention()).setFooter("React with a \uD83D\uDED1 within 10 seconds to stop the stop request!", null).build())
                                    .onClick("\uD83D\uDED1", (click, user) -> {
                                        sendStop.set(false);
                                        menu[0].removeReaction("\uD83D\uDED1");
                                        menu[0].getMessage().setContent(new EmbedBuilder().setColor(Color.RED).setTitle("Stop Cancelled!").setDescription("The stop for `" + server.getName() + "` failed to send because it was cancelled by " + user.getAsMention() + "!").build());
                                    });
                            builder.onDisplay(display -> {
                                scheduledTask.schedule(() -> {
                                    if (sendStop.get()) {
                                        if (ActionCommand.doAction(bot, "stop", server.getIdentifier())) {
                                            menu[0].removeReaction("\uD83D\uDED1");
                                            menu[0].getMessage().setContent(new EmbedBuilder().setTitle("Stopped!").setColor(Color.GREEN).setDescription("The stop was successfully sent to `" + server.getName() + "` as requested by " + member.getAsMention()).build());
                                        } else {
                                            menu[0].getMessage().setContent(new EmbedBuilder().setTitle("Stop Failed!").setColor(Color.RED).setDescription("The stop failed to send to `" + server.getName() + "` as requested by " + member.getAsMention()).build());
                                        }
                                    }
                                }, 10, TimeUnit.SECONDS);
                            });
                            menu[0] = builder.buildAndDisplay(channel);
                            menu[0].destroyIn(20);
                            return CommandResult.success();
                        } else if (matchedServers.size() > 1) {
                            final ReactionMenu[] builtMenu = {null};
                            ReactionMenu.Builder menuBuilder = new ReactionMenu.Builder(bot.getJda());
                            EmbedBuilder embed = new EmbedBuilder().setTitle("Multiple Matches Found!").setColor(Color.CYAN);
                            embed.setDescription("To pick the match you want, please react with the emoji appertaining to the server name!");
                            char endCharacter = 'a';
                            String alias = "regional_indicator_symbol_";
                            for (Server server : matchedServers) {
                                Emoji emoji = EmojiManager.getForAlias(alias + endCharacter);
                                embed.addField(server.getName() + ": ", emoji.getUnicode(), true);
                                endCharacter++;
                                menuBuilder.onClick("\u274C", ReactionMenu::destroy);
                                menuBuilder.onClick(emoji.getUnicode(), (react, user) -> {
                                    AtomicBoolean sendStop = new AtomicBoolean(true);
                                    final ReactionMenu[] menu = {null};
                                    ReactionMenu.Builder builder = new ReactionMenu.Builder(bot.getJda());
                                    builder.setEmbed(new EmbedBuilder().setTitle("Stopping...").setColor(Color.ORANGE).setDescription("Sending a stop request to `" + server.getName() + "` as requested by " + member.getAsMention()).setFooter("React with a \uD83D\uDED1 within 10 seconds to stop the stop request!", null).build())
                                            .onClick("\uD83D\uDED1", (click, clickedUser) -> {
                                                sendStop.set(false);
                                                menu[0].removeReaction("\uD83D\uDED1");
                                                menu[0].getMessage().setContent(new EmbedBuilder().setColor(Color.RED).setTitle("Stop Cancelled!").setDescription("The stop for `" + server.getName() + "` failed to send because it was cancelled by " + clickedUser.getAsMention() + "!").build());
                                            });
                                    builder.onDisplay(display -> {
                                        scheduledTask.schedule(() -> {
                                            if (sendStop.get()) {
                                                if (ActionCommand.doAction(bot, "stop", server.getIdentifier())) {
                                                    menu[0].removeReaction("\uD83D\uDED1");
                                                    menu[0].getMessage().setContent(new EmbedBuilder().setTitle("Stopped!").setColor(Color.GREEN).setDescription("The stop was successfully sent to `" + server.getName() + "` as requested by " + member.getAsMention()).build());
                                                } else {
                                                    menu[0].getMessage().setContent(new EmbedBuilder().setTitle("Stop Failed!").setColor(Color.RED).setDescription("The stop failed to send to `" + server.getName() + "` as requested by " + member.getAsMention()).build());
                                                }
                                            }
                                        }, 10, TimeUnit.SECONDS);
                                    });
                                    menu[0] = builder.buildAndDisplay(channel);
                                    menu[0].destroyIn(20);
                                    if (builtMenu[0] != null)
                                        builtMenu[0].destroy();
                                });
                            }
                            menuBuilder.setEmbed(embed.build());
                            builtMenu[0] = menuBuilder.buildAndDisplay(channel);
                            builtMenu[0].destroyIn(20);
                            return CommandResult.success();
                        } else {
                            bot.getMessenger().sendEmbed(channel, new EmbedBuilder().setColor(Color.RED).setTitle("Error").setDescription("We couldn't find a server with a name close enough to: `" + serverArg + "`").addField("Tip:", "Run `" + bot.getMainConfig().getConfigValue("prefix").getAsString() + "servers` to see a list of servers available for you to restart!", true).build(), 15);
                            return CommandResult.success();
                        }
                    } else {
                        Role role = bot.getJda().getRoleById(bot.getMainConfig().getConfigValue("statusRoleId").getAsLong());
                        bot.getMessenger().sendEmbed(channel, new EmbedBuilder().setTitle("Error").setDescription("You do not have the required role: `" + role.getName() + "` to use this command!").setColor(Color.RED).build(), 10);
                        return CommandResult.success();
                    }
                } else {
                    bot.getMessenger().sendEmbed(channel, new EmbedBuilder().setColor(Color.MAGENTA).setTitle("Stop failed!").setDescription("The stop failed to send because the bot is in lockdown. This was enabled because a member was potentially compromised and was abusing the bot!").setFooter("To disable lockdown a staff member will need to run 'lockdown' in the bots console!", null).build(), 10);
                    return CommandResult.success();
                }

            }
            return CommandResult.invalidArguments();
        }
    }

    @Command(label = "start", description = "When provided with a server it will send a request to that server for it to start itself!", usage = "start <server>")
    public static class Start {

        @Execute
        public CommandResult onStart(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowBotStaff bot) {
            if (!args.isEmpty()) {
                if (!bot.getLockdownStatus()) {
                    if (member.getRoles().stream().map(Role::getIdLong).anyMatch((role) -> role == bot.getMainConfig().getConfigValue("statusRoleId").getAsLong())) {
                        String serverArg = String.join(" ", args);
                        ArrayList<Server> matchedServers = new ArrayList<>();
                        int lowest = 100;
                        for (Server server : bot.getAvailableServers()) {
                            int maybe = Util.computeLevenshteinDistance(server.getName(), serverArg);
                            if (maybe < lowest) {
                                lowest = maybe;
                                matchedServers = new ArrayList<>();
                                matchedServers.add(server);
                            } else if (maybe == lowest) {
                                matchedServers.add(server);
                            }
                        }
                        if (matchedServers.size() == 1) {
                            AtomicBoolean sendStart = new AtomicBoolean(true);
                            Server server = matchedServers.get(0);
                            final ReactionMenu[] menu = {null};
                            ReactionMenu.Builder builder = new ReactionMenu.Builder(bot.getJda());
                            builder.setEmbed(new EmbedBuilder().setTitle("Starting...").setColor(Color.ORANGE).setDescription("Sending a start request to `" + server.getName() + "` as requested by " + member.getAsMention()).setFooter("React with a \uD83D\uDED1 within 10 seconds to stop the start request!", null).build())
                                    .onClick("\uD83D\uDED1", (click, user) -> {
                                        sendStart.set(false);
                                        menu[0].removeReaction("\uD83D\uDED1");
                                        menu[0].getMessage().setContent(new EmbedBuilder().setColor(Color.RED).setTitle("Start Cancelled!").setDescription("The start for `" + server.getName() + "` failed to send because it was cancelled by " + user.getAsMention() + "!").build());
                                    });
                            builder.onDisplay(display -> {
                                scheduledTask.schedule(() -> {
                                    if (sendStart.get()) {
                                        if (ActionCommand.doAction(bot, "start", server.getIdentifier())) {
                                            menu[0].removeReaction("\uD83D\uDED1");
                                            menu[0].getMessage().setContent(new EmbedBuilder().setTitle("Started!").setColor(Color.GREEN).setDescription("The start was successfully sent to `" + server.getName() + "` as requested by " + member.getAsMention()).build());
                                        } else {
                                            menu[0].getMessage().setContent(new EmbedBuilder().setTitle("Start Failed!").setColor(Color.RED).setDescription("The start failed to send to `" + server.getName() + "` as requested by " + member.getAsMention()).build());
                                        }
                                    }
                                }, 10, TimeUnit.SECONDS);
                            });
                            menu[0] = builder.buildAndDisplay(channel);
                            menu[0].destroyIn(20);
                            return CommandResult.success();
                        } else if (matchedServers.size() > 1) {
                            final ReactionMenu[] builtMenu = {null};
                            ReactionMenu.Builder menuBuilder = new ReactionMenu.Builder(bot.getJda());
                            EmbedBuilder embed = new EmbedBuilder().setTitle("Multiple Matches Found!").setColor(Color.CYAN);
                            embed.setDescription("To pick the match you want, please react with the emoji appertaining to the server name!");
                            char endCharacter = 'a';
                            String alias = "regional_indicator_symbol_";
                            for (Server server : matchedServers) {
                                Emoji emoji = EmojiManager.getForAlias(alias + endCharacter);
                                embed.addField(server.getName() + ": ", emoji.getUnicode(), true);
                                endCharacter++;
                                menuBuilder.onClick("\u274C", ReactionMenu::destroy);
                                menuBuilder.onClick(emoji.getUnicode(), (react, user) -> {
                                    AtomicBoolean sendStop = new AtomicBoolean(true);
                                    final ReactionMenu[] menu = {null};
                                    ReactionMenu.Builder builder = new ReactionMenu.Builder(bot.getJda());
                                    builder.setEmbed(new EmbedBuilder().setTitle("Starting...").setColor(Color.ORANGE).setDescription("Sending a start request to `" + server.getName() + "` as requested by " + member.getAsMention()).setFooter("React with a \uD83D\uDED1 within 10 seconds to stop the start request!", null).build())
                                            .onClick("\uD83D\uDED1", (click, clickedUser) -> {
                                                sendStop.set(false);
                                                menu[0].removeReaction("\uD83D\uDED1");
                                                menu[0].getMessage().setContent(new EmbedBuilder().setColor(Color.RED).setTitle("Start Cancelled!").setDescription("The start for `" + server.getName() + "` failed to send because it was cancelled by " + clickedUser.getAsMention() + "!").build());
                                            });
                                    builder.onDisplay(display -> {
                                        scheduledTask.schedule(() -> {
                                            if (sendStop.get()) {
                                                if (ActionCommand.doAction(bot, "start", server.getIdentifier())) {
                                                    menu[0].removeReaction("\uD83D\uDED1");
                                                    menu[0].getMessage().setContent(new EmbedBuilder().setTitle("Started!").setColor(Color.GREEN).setDescription("The start was successfully sent to `" + server.getName() + "` as requested by " + member.getAsMention()).build());
                                                } else {
                                                    menu[0].getMessage().setContent(new EmbedBuilder().setTitle("Start Failed!").setColor(Color.RED).setDescription("The start failed to send to `" + server.getName() + "` as requested by " + member.getAsMention()).build());
                                                }
                                            }
                                        }, 10, TimeUnit.SECONDS);
                                    });
                                    menu[0] = builder.buildAndDisplay(channel);
                                    menu[0].destroyIn(20);
                                    if (builtMenu[0] != null)
                                        builtMenu[0].destroy();
                                });
                            }
                            menuBuilder.setEmbed(embed.build());
                            builtMenu[0] = menuBuilder.buildAndDisplay(channel);
                            builtMenu[0].destroyIn(20);
                            return CommandResult.success();
                        } else {
                            bot.getMessenger().sendEmbed(channel, new EmbedBuilder().setColor(Color.RED).setTitle("Error").setDescription("We couldn't find a server with a name close enough to: `" + serverArg + "`").addField("Tip:", "Run `" + bot.getMainConfig().getConfigValue("prefix").getAsString() + "servers` to see a list of servers available for you to restart!", true).build(), 15);
                            return CommandResult.success();
                        }
                    } else {
                        Role role = bot.getJda().getRoleById(bot.getMainConfig().getConfigValue("statusRoleId").getAsLong());
                        bot.getMessenger().sendEmbed(channel, new EmbedBuilder().setTitle("Error").setDescription("You do not have the required role: `" + role.getName() + "` to use this command!").setColor(Color.RED).build(), 10);
                        return CommandResult.success();
                    }
                } else {
                    bot.getMessenger().sendEmbed(channel, new EmbedBuilder().setColor(Color.MAGENTA).setTitle("Start failed!").setDescription("The start failed to send because the bot is in lockdown. This was enabled because a member was potentially compromised and was abusing the bot!").setFooter("To disable lockdown a staff member will need to run 'lockdown' in the bots console!", null).build(), 10);
                    return CommandResult.success();
                }
            }
            return CommandResult.invalidArguments();
        }
    }
}
