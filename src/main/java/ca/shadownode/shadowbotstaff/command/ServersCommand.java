package ca.shadownode.shadowbotstaff.command;

import ca.shadownode.shadowbotstaff.ShadowBotStaff;
import ca.shadownode.shadowbotstaff.logging.Logger;
import ca.shadownode.shadowbotstaff.pagination.Page;
import ca.shadownode.shadowbotstaff.pagination.PageBuilder;
import ca.shadownode.shadowbotstaff.pagination.PaginationEmbed;
import ca.shadownode.shadowbotstaff.utilities.Server;
import ca.shadownode.shadowbotstaff.utilities.ServerInfo;
import ca.shadownode.shadowbotstaff.utilities.Util;
import me.bhop.bjdautilities.ReactionMenu;
import me.bhop.bjdautilities.command.annotation.Command;
import me.bhop.bjdautilities.command.annotation.Execute;
import me.bhop.bjdautilities.command.result.CommandResult;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.TextChannel;

import java.awt.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Command(label = "servers", description = "When ran it will get you the list of all available servers!", usage = "servers")
public class ServersCommand {

    @Execute
    public CommandResult onServers(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowBotStaff bot) {
        int maxPages = bot.getAvailableServers().size() % 3 == 0 ? bot.getAvailableServers().size() / 3 : bot.getAvailableServers().size() / 3 + 1;
        int count = 1;
        int page = 1;
        List<Page> content = new ArrayList<>();
        PaginationEmbed.Builder builder = new PaginationEmbed.Builder(bot.getJda());
        while (count <= maxPages) {
            content.add(generatePage(page, maxPages, bot.getAvailableServers().stream().skip((count - 1) * 3).limit(3), member, bot));
            count++;
            if (page < maxPages)
                page++;
        }
        content.forEach(builder::addPage);
        builder.buildAndDisplay(channel);
        return CommandResult.success();
    }

    private Page generatePage(int page, int maxPages, Stream<Server> servers, Member sender, ShadowBotStaff bot) {
        PageBuilder pageBuilder = new PageBuilder().setEntryLimit(3).includeTimestamp(true).setColor(Color.CYAN).setTitle("**Available Servers**");
        pageBuilder.setFooter("Page " + page + " of " + maxPages, sender.getJDA().getSelfUser().getAvatarUrl());
        servers.forEach(server -> {
            ServerInfo info = Util.getServerInfo(bot, server.getIdentifier());
            pageBuilder.addContent(false, "**" + server.getName() + "**",
            "\u2022\u0020**Online:** " + (info.getOnline() ? "\u2705" : "\u274C"),
            "\u2022\u0020**CPU Usage:** " + info.getCpuPercentage() + "%",
            "\u2022\u0020**Disk Usage:** " + info.getDiskUsage() + "MB",
            "\u2022\u0020**Memory Usage:** " + info.getMemoryUsage() + "MB");
        });
        return pageBuilder.build();
    }
}
