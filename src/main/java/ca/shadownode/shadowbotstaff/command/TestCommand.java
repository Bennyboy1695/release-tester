package ca.shadownode.shadowbotstaff.command;

import ca.shadownode.shadowbotstaff.ShadowBotStaff;
import ca.shadownode.shadowbotstaff.pagination.Page;
import ca.shadownode.shadowbotstaff.pagination.PageBuilder;
import ca.shadownode.shadowbotstaff.pagination.PaginationEmbed;
import me.bhop.bjdautilities.command.annotation.Command;
import me.bhop.bjdautilities.command.annotation.Execute;
import me.bhop.bjdautilities.command.result.CommandResult;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

import java.awt.*;
import java.util.List;

@Command(value = "test", description = "Used for testing certain things before they get made into actual things!")
public class TestCommand {

    @Execute
    public CommandResult onTest(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowBotStaff bot) {
        Page page1 = new PageBuilder()
                .setTitle("Page 1").setDescription("This is a separate description for every page!")
                .setColor(Color.RED).addContent(false, "Color", "the color can change per page too!").build();
        Page page2 = new PageBuilder()
                .setTitle("Page 2 with a link!", "https://google.com")
                .setColor(Color.GREEN).addContent(false, "color", "my color changed :O")
                .includeTimestamp(true).setFooter("And i have a timestamp and footer!").build();

        PaginationEmbed paginationEmbed = new PaginationEmbed.Builder(bot.getJda()).addPage(page1).addPage(page2).build();
        paginationEmbed.display(channel);

        return CommandResult.success();
    }
}
