package ca.shadownode.shadowbotstaff.command;

import ca.shadownode.shadowbotstaff.logging.Logger;
import ca.shadownode.shadowbotstaff.ShadowBotStaff;
import com.google.gson.JsonObject;
import me.bhop.bjdautilities.command.annotation.Command;
import me.bhop.bjdautilities.command.annotation.Execute;
import me.bhop.bjdautilities.command.result.CommandResult;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

import java.awt.*;
import java.util.List;

@Command(label = "lockdown", description = "When the command is ran it will make it so the start, stop and restart commands can no longer be ran until someone has de-activated lockdown mode. Lockdown is to stop those command sin the case of a compromised account abusing them", usage = "lockdown")
public class LockdownCommand {

    @Execute
    public CommandResult onLockdown(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowBotStaff bot) {
        JsonObject config = bot.getMainConf();
        JsonObject object = config.get("NO_TOUCH").getAsJsonObject();
        if (object.get("inlockdown").getAsBoolean()) {
            bot.getMessenger().sendEmbed(channel, new EmbedBuilder().setColor(Color.RED).setTitle("Error").setDescription("Lockdown cannot be disabled within discord. Please go to the bots console and run `lockdown`").build(), 10);
        } else if (!object.get("inlockdown").getAsBoolean()) {
            object.addProperty("inlockdown", true);
            bot.getMessenger().sendEmbed(channel, new EmbedBuilder().setColor(Color.MAGENTA).setTitle("Lockdown enabled!").setDescription("You have successfully enabled lockdown mode! This means nobody can run the status commands until it has been re-enabled!").build(), 10);
            Logger.warn("Lockdown has been enabled by " + member.getEffectiveName() + "!");
        }
        bot.getMainConfig().saveConfig("config", config);
        bot.reloadConfig();
        return CommandResult.success();
    }
}
