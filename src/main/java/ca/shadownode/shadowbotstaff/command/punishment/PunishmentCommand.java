package ca.shadownode.shadowbotstaff.command.punishment;

import ca.shadownode.shadowbotstaff.ShadowBotStaff;
import ca.shadownode.shadowbotstaff.utilities.Server;
import ca.shadownode.shadowbotstaff.utilities.Util;
import me.bhop.bjdautilities.ReactionMenu;
import me.bhop.bjdautilities.command.annotation.Command;
import me.bhop.bjdautilities.command.annotation.Execute;
import me.bhop.bjdautilities.command.result.CommandResult;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.exceptions.ErrorResponseException;
import net.dv8tion.jda.core.utils.tuple.Pair;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Command(label = {"punish", "punishment"}, description = "Starts a punishment report builder in your private messages!")
public class PunishmentCommand {

    private final ExecutorService executorService = Executors.newScheduledThreadPool(2);
    private EmbedBuilder punishmentEmbedBuilder = new EmbedBuilder().setColor(Color.decode("#ee82ee"));
    ArrayList<String> messageIds = new ArrayList<>();
    final boolean[] staffError = {false};
    final boolean[] serverError = {false};
    final boolean[] punishmentError = {false};
    final boolean[] hasClickedStaffNames = {false};
    final boolean[] usernameError = {false};
    final boolean[] reasonError = {false};
    final boolean[] evidenceError = {false};
    final boolean[] sentPmInfo = {false};
    final boolean[] hasClickedUsernames = {false};
    final boolean[] hasClickedServers = {false};
    final boolean[] hasClickedPunishments = {false};
    final boolean[] hasClickedReasons = {false};
    final boolean[] hasClickedEvidence = {false};
    final boolean[] hasClickedInfo = {false};

    @Execute
    public CommandResult onPunishment(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowBotStaff bot) {
        messageIds = new ArrayList<>();
        hasClickedUsernames[0] = false;
        hasClickedStaffNames[0] = false;
        hasClickedServers[0] = false;
        hasClickedPunishments[0] = false;
        hasClickedReasons[0] = false;
        hasClickedInfo[0] = false;
        hasClickedEvidence[0] = false;
        sentPmInfo[0] = false;
        usernameError[0] = false;
        staffError[0] = false;
        serverError[0] = false;
        punishmentError[0] = false;
        reasonError[0] = false;
        evidenceError[0] = false;
        punishmentEmbedBuilder = new EmbedBuilder().setColor(Color.decode("#ee82ee"));

        ReactionMenu menu = new ReactionMenu.Builder(bot.getJda())
                .setEmbed(new EmbedBuilder()
                        .setColor(Color.CYAN)
                        .setTitle("Punishment")
                        .setDescription("This embed will be your help guide to all things punishment related!")
                        .addField("Create", "To create a new punishment entry please react to this with a \uD83C\uDD95", false)
                        .addField("Edit", "To edit an already existing punishment you can either react to that embed with a \uD83D\uDCDD or run " + bot.getMainConfig().getConfigValue("prefix").getAsString() + "punishment edit <username>", false)
                        .build())
                .onClick("\uD83C\uDD95", (create, user) -> {
                    user.openPrivateChannel().queue((success) -> {
                        playerUsernames(bot, success, channel);
                        if (!sentPmInfo[0])
                            executorService.submit(() -> {
                                bot.getMessenger().sendEmbed(channel, new EmbedBuilder().setTitle("Creation Started!").setDescription(user.getAsMention() + ": A private message has been sent to you to start the creation process!").addField("Private Message", "https://discordapp.com/channels/@me/" + success.getIdLong(), false).build(), 20);
                            });
                    });
                }).buildAndDisplay(channel);
        menu.destroyIn(20);
        return CommandResult.success();
    }

    private void playerUsernames(ShadowBotStaff bot, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    sentPmInfo[0] = true;
                    ArrayList<Message> beforeBotMessages = new ArrayList<>();
                    ReactionMenu pmReaction = new ReactionMenu.PrivateMessageReactionMenu.Builder(bot.getJda()).setEmbed(
                            (usernameError[0] ? new EmbedBuilder().setTitle("Error").setColor(Color.RED).setDescription("You didn't specify any usernames! \n Please enter the username(s) of the user(s) being punished!").setFooter("Progress: 0%", null).build()
                                    : new EmbedBuilder().setTitle("Punishment Creation: Step One").setColor(Color.decode("#04E11E")).setDescription("Please enter the username(s) of the user(s) being punished!").setFooter("Progress: 0%", null).build()))
                            .onClick("\u2705", (done, user1) -> {
                                if (!hasClickedUsernames[0]) {
                                    success.getHistory().retrievePast(20).queue((history) -> {
                                        for (Message messages : history) {
                                            if (messages.getAuthor().isBot())
                                                break;
                                            beforeBotMessages.add(messages);
                                        }
                                        if (!beforeBotMessages.isEmpty()) {
                                            if (beforeBotMessages.size() == 1) {
                                                String msg = beforeBotMessages.get(0).getContentRaw();
                                                String[] split = msg.split("[,]|[ ]");
                                                List<String> usernames = Arrays.stream(split).filter(s -> !s.isEmpty()).collect(Collectors.toList());
                                                for (String name : usernames) {
                                                    Pair pair = Util.getPlayerInfoFromUsername(name.replace(" ", ""));
                                                    if (pair != null) {
                                                        executorService.submit(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                punishmentEmbedBuilder = punishmentEmbedBuilder
                                                                        .setTitle("**" + pair.getRight().toString().replace("_", "\\_") + "** Punishment Record")
                                                                        .setDescription("**UUID:** " + ("[" + pair.getLeft() + "](https://namemc.com/search?q=" + pair.getLeft() + ")"))
                                                                        .setThumbnail("https://mc-heads.net/avatar/" + pair.getLeft().toString() + ".png");
                                                                ReactionMenu punishEmbed = new ReactionMenu.Builder(bot.getJda())
                                                                        .setEmbed(punishmentEmbedBuilder.build())
                                                                        .setStartingReactions("\uD83D\uDCDD")
                                                                        .buildAndDisplay(channel);
                                                                bot.getMongoRequestRegistry().createNewPunishment(String.valueOf(punishEmbed.getMessage().getIdLong()), pair.getLeft().toString());
                                                                messageIds.add(String.valueOf(punishEmbed.getMessage().getIdLong()));
                                                            }
                                                        });
                                                    } else {
                                                        executorService.submit(() -> {
                                                            bot.getMessenger().sendEmbed(success, new EmbedBuilder().setColor(Color.RED).setTitle("Error").setDescription("The username: `" + name + "` isn't a valid username! Please type the usernames again making sure they are correctly spelt.").build(), 10);
                                                            usernameError[0] = true;
                                                            this.run();
                                                        });
                                                    }
                                                }
                                            } else {
                                                for (Message usernameMessages : beforeBotMessages) {
                                                    Pair pair = Util.getPlayerInfoFromUsername(usernameMessages.getContentRaw().replace(" ", ""));
                                                    if (pair != null) {
                                                        executorService.submit(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                punishmentEmbedBuilder = punishmentEmbedBuilder
                                                                        .setTitle("**" + pair.getRight().toString().replace("_", "\\_") + "** Punishment Record")
                                                                        .setDescription("**UUID:** " + ("[" + pair.getLeft() + "](https://namemc.com/search?q=" + pair.getLeft() + ")"))
                                                                        .setThumbnail("https://mc-heads.net/avatar/" + pair.getLeft().toString() + ".png");
                                                                ReactionMenu punishEmbed = new ReactionMenu.Builder(bot.getJda())
                                                                        .setEmbed(punishmentEmbedBuilder.build())
                                                                        .setStartingReactions("\uD83D\uDCDD")
                                                                        .buildAndDisplay(channel);
                                                                bot.getMongoRequestRegistry().createNewPunishment(String.valueOf(punishEmbed.getMessage().getIdLong()), pair.getLeft().toString());
                                                                messageIds.add(String.valueOf(punishEmbed.getMessage().getIdLong()));
                                                            }
                                                        });
                                                    } else {
                                                        executorService.submit(() -> {
                                                            bot.getMessenger().sendEmbed(success, new EmbedBuilder().setColor(Color.RED).setTitle("Error").setDescription("The username: `" + usernameMessages.getContentRaw() + "` isn't a valid username! Please type the usernames again making sure they are correctly spelt.").build(), 10);
                                                            usernameError[0] = true;
                                                            this.run();
                                                        });
                                                    }
                                                }
                                            }
                                            if (!usernameError[0])
                                                staffUsernames(bot, success, channel);
                                        } else {
                                            usernameError[0] = true;
                                            this.run();
                                        }
                                    });
                                    hasClickedUsernames[0] = true;
                                }
                            })
                            .buildAndDisplayForPrivateMessage(success);
                } catch (ErrorResponseException e) {
                    sentPmInfo[0] = false;
                    bot.getMessenger().sendEmbed(channel, new EmbedBuilder().setTitle("Error").setColor(Color.RED).setDescription("You do not have private messages open, so the bot was not able to message you to start a punishment creation process!").build(), 20);
                }
            }
        });
    }

    private void staffUsernames(ShadowBotStaff bot, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<Message> staffHistory = new ArrayList<>();
                ReactionMenu pmStaffReaction = new ReactionMenu.Builder(bot.getJda())
                        .setEmbed(new EmbedBuilder().setTitle("Punishment Creation: Step Two").setColor(Color.decode("#04E11E")).setDescription("Please enter the discord username(s) of the staff member(s) that helped you with this punishment!\n You will be added automatically so if it was only you click the checkmark.").setFooter("Progress: 20%", null).build())
                        .onClick("\u2705", (done, staff1) -> {
                            if (!hasClickedStaffNames[0]) {
                                success.getHistory().retrievePast(20).queue((history) -> {
                                    for (Message messages : history) {
                                        if (messages.getAuthor().isBot())
                                            break;
                                        staffHistory.add(messages);
                                    }
                                    if (!staffHistory.isEmpty()) {
                                        if (staffHistory.size() == 1) {
                                            String msg = staffHistory.get(0).getContentRaw();
                                            String[] split = msg.split("[,]|[ ]");
                                            List<String> usernames = Arrays.stream(split).filter(s -> !s.isEmpty()).collect(Collectors.toList());
                                            List<String> matchedMembers = new ArrayList<>();
                                            int lowest = 100;
                                            for (String str : usernames) {
                                                for (Member staffMember : channel.getGuild().getMembers()) {
                                                    int maybe = Util.computeLevenshteinDistance(str.toLowerCase(), staffMember.getEffectiveName().toLowerCase());
                                                    if (maybe < lowest) {
                                                        lowest = maybe;
                                                        matchedMembers = new ArrayList<>();
                                                        matchedMembers.add(String.valueOf(staffMember.getUser().getIdLong()));
                                                    } else if (maybe == lowest) {
                                                        matchedMembers.add(String.valueOf(staffMember.getUser().getIdLong()));
                                                    }
                                                }
                                            }
                                            if (matchedMembers.size() == 1) {
                                                List<String> finalMatchedMembers = matchedMembers;
                                                StringBuilder builder = new StringBuilder(staff1.getAsMention()).append(", ");
                                                finalMatchedMembers.forEach(str -> builder.append(channel.getGuild().getMember(bot.getJda().getUserById(str)).getAsMention()));
                                                finalMatchedMembers.add(staff1.getId());
                                                messageIds.forEach(id -> bot.getMongoRequestRegistry().addStaffToPunishment(id, finalMatchedMembers));
                                                messageIds.forEach(id -> {
                                                    channel.getMessageById(id).queue(editMsg -> {
                                                        punishmentEmbedBuilder = punishmentEmbedBuilder.appendDescription("\n**Staff Attended:** " + builder.toString());
                                                        executorService.submit(() -> editMsg.editMessage(punishmentEmbedBuilder.build()).queue());
                                                    });
                                                });
                                            } else {
                                                int lowest1 = 100;
                                                List<String> matchedMembers1 = new ArrayList<>();
                                                for (Message msg1 : staffHistory) {
                                                    for (Member staffMember : channel.getGuild().getMembers()) {
                                                        int maybe = Util.computeLevenshteinDistance(msg1.getContentRaw().toLowerCase(), staffMember.getEffectiveName().toLowerCase());
                                                        if (maybe < lowest1) {
                                                            lowest1 = maybe;
                                                            matchedMembers1 = new ArrayList<>();
                                                            matchedMembers1.add(String.valueOf(staffMember.getUser().getIdLong()));
                                                        } else if (maybe == lowest1) {
                                                            matchedMembers1.add(String.valueOf(staffMember.getUser().getIdLong()));
                                                        }
                                                    }
                                                }
                                                StringBuilder builder = new StringBuilder(staff1.getAsMention()).append(", ");
                                                List<String> finalMatchedMembers = matchedMembers1;
                                                for (String member : matchedMembers1) {
                                                    builder.append(channel.getGuild().getMember(bot.getJda().getUserById(member)).getAsMention()).append(", ");
                                                }
                                                finalMatchedMembers.add(staff1.getId());
                                                messageIds.forEach(id -> bot.getMongoRequestRegistry().addStaffToPunishment(id, finalMatchedMembers));
                                                messageIds.forEach(id -> {
                                                    channel.getMessageById(id).queue(editMsg -> {
                                                        punishmentEmbedBuilder = punishmentEmbedBuilder.appendDescription("\n**Staff Attended:** " + builder.toString().substring(0, builder.toString().length() - 2));
                                                        executorService.submit(() -> editMsg.editMessage(punishmentEmbedBuilder.build()).queue());
                                                    });
                                                });
                                            }
                                        } else {
                                            int lowest = 100;
                                            List<String> matchedMembers = new ArrayList<>();
                                            for (Message msg : staffHistory) {
                                                for (Member staffMember : channel.getGuild().getMembers()) {
                                                    int maybe = Util.computeLevenshteinDistance(msg.getContentRaw().toLowerCase(), staffMember.getEffectiveName().toLowerCase());
                                                    if (maybe < lowest) {
                                                        lowest = maybe;
                                                        matchedMembers = new ArrayList<>();
                                                        matchedMembers.add(String.valueOf(staffMember.getUser().getIdLong()));
                                                    } else if (maybe == lowest) {
                                                        matchedMembers.add(String.valueOf(staffMember.getUser().getIdLong()));
                                                    }
                                                }
                                            }
                                            StringBuilder builder = new StringBuilder(staff1.getAsMention()).append(", ");
                                            List<String> finalMatchedMembers = matchedMembers;
                                            for (String member : matchedMembers) {
                                                builder.append(channel.getGuild().getMember(bot.getJda().getUserById(member)).getAsMention()).append(", ");
                                            }
                                            finalMatchedMembers.add(staff1.getId());
                                            messageIds.forEach(id -> bot.getMongoRequestRegistry().addStaffToPunishment(id, finalMatchedMembers));
                                            messageIds.forEach(id -> {
                                                channel.getMessageById(id).queue(editMsg -> {
                                                    punishmentEmbedBuilder = punishmentEmbedBuilder.appendDescription("\n**Staff Attended:** " + builder.toString().substring(0, builder.toString().length() - 2));
                                                    executorService.submit(() -> editMsg.editMessage(punishmentEmbedBuilder.build()).queue());
                                                });
                                            });
                                        }
                                        if (!staffError[0])
                                            serversStep(bot, success, channel);
                                    } else {
                                        messageIds.forEach(id -> bot.getMongoRequestRegistry().addStaffToPunishment(id, Arrays.asList(String.valueOf(staff1.getIdLong()))));
                                        messageIds.forEach(id -> {
                                            channel.getMessageById(id).queue(editMsg -> {
                                                punishmentEmbedBuilder = punishmentEmbedBuilder.appendDescription("\n**Staff Attended:** " + staff1.getAsMention());
                                                executorService.submit(() -> editMsg.editMessage(punishmentEmbedBuilder.build()).queue());
                                            });
                                        });
                                        serversStep(bot, success, channel);
                                    }
                                });
                                hasClickedStaffNames[0] = true;
                            }
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void serversStep(ShadowBotStaff bot, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<Message> serverHistory = new ArrayList<>();
                ReactionMenu serverPm = new ReactionMenu.Builder(bot.getJda())
                        .setEmbed((serverError[0] ? new EmbedBuilder().setTitle("Error").setColor(Color.RED).setDescription("You didn't specify a server! \n Please enter the server(s) the offence occurred on!\n Type `global` if it was a global punishment.").setFooter("Progress: 40%", null).build()
                                : new EmbedBuilder().setTitle("Punishment Creation: Step Three").setColor(Color.decode("#04E11E")).setDescription("Please enter the server(s) the offence occurred on!\n Type `global` if it was a global punishment.").setFooter("Progress: 40%", null).build()))
                        .onClick("\u2705", (done, server1) -> {
                            if (!hasClickedServers[0]) {
                                success.getHistory().retrievePast(20).queue((history) -> {
                                    for (Message messages : history) {
                                        if (messages.getAuthor().isBot())
                                            break;
                                        serverHistory.add(messages);
                                    }
                                    if (!serverHistory.isEmpty()) {
                                        if (serverHistory.size() == 1) {
                                            String msg = serverHistory.get(0).getContentRaw();
                                            String[] split = msg.split("[,]|[ ]");
                                            List<String> servers = Arrays.stream(split).filter(s -> !s.isEmpty()).collect(Collectors.toList());
                                            if (servers.size() == 1 && servers.get(0).equalsIgnoreCase("global")) {
                                                messageIds.forEach(id -> bot.getMongoRequestRegistry().addServersToPunishment(id, Arrays.asList("Global")));
                                                messageIds.forEach(id -> {
                                                    channel.getMessageById(id).queue(editMsg -> {
                                                        punishmentEmbedBuilder = punishmentEmbedBuilder.appendDescription("\n**Servers:** " + "Global");
                                                        executorService.submit(() -> editMsg.editMessage(punishmentEmbedBuilder.build()).queue());
                                                    });
                                                });
                                            } else {
                                                List<String> matchedServers = new ArrayList<>();
                                                int lowest = 100;
                                                for (String str : servers) {
                                                    for (Server server : bot.getAvailableServers()) {
                                                        int maybe = Util.computeLevenshteinDistance(str, server.getName());
                                                        if (maybe < lowest) {
                                                            lowest = maybe;
                                                            matchedServers = new ArrayList<>();
                                                            matchedServers.add(String.valueOf(server.getName()));
                                                        } else if (maybe == lowest) {
                                                            matchedServers.add(String.valueOf(server.getName()));
                                                        }
                                                    }
                                                }
                                                if (matchedServers.size() == 1) {
                                                    List<String> finalMatchedServers = matchedServers;
                                                    StringBuilder builder = new StringBuilder();
                                                    finalMatchedServers.forEach(builder::append);
                                                    messageIds.forEach(id -> bot.getMongoRequestRegistry().addServersToPunishment(id, finalMatchedServers));
                                                    messageIds.forEach(id -> {
                                                        channel.getMessageById(id).queue(editMsg -> {
                                                            punishmentEmbedBuilder = punishmentEmbedBuilder.appendDescription("\n**Servers:** " + builder.toString());
                                                            executorService.submit(() -> editMsg.editMessage(punishmentEmbedBuilder.build()).queue());
                                                        });
                                                    });
                                                } else {

                                                }
                                            }
                                        }
                                        if (!serverError[0])
                                            punishmentStep(bot, success, channel);
                                    } else {
                                        serverError[0] = true;
                                        this.run();
                                    }
                                });
                                hasClickedServers[0] = true;
                            }
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void punishmentStep(ShadowBotStaff bot, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<Message> punishmentHistory = new ArrayList<>();
                ReactionMenu punishmentPm = new ReactionMenu.Builder(bot.getJda())
                        .setEmbed((punishmentError[0] ? new EmbedBuilder().setTitle("Error").setColor(Color.RED).setDescription("You didn't specify any punishments! \n Please enter the punishment(s) the player(s) received!").addField("", "Please use comma's or seperate messages to seperate punishments!", false).setFooter("Progress: 60%", null).build()
                                : new EmbedBuilder().setTitle("Punishment Creation: Step Four").setColor(Color.decode("#04E11E")).setDescription("Please enter the punishment(s) the player(s) received!").addField("", "Please use comma's or seperate messages to seperate punishments!", false).setFooter("Progress: 60%", null).build()))
                        .onClick("\u2705", (done, server1) -> {
                            if (!hasClickedPunishments[0]) {
                                success.getHistory().retrievePast(20).queue((history) -> {
                                    for (Message messages : history) {
                                        if (messages.getAuthor().isBot())
                                            break;
                                        punishmentHistory.add(messages);
                                    }
                                    if (!punishmentHistory.isEmpty()) {
                                        if (punishmentHistory.size() == 1) {
                                            String msg = punishmentHistory.get(0).getContentRaw();
                                            String[] split = msg.split(", ");
                                            List<String> punishments = Arrays.stream(split).filter(s -> !s.isEmpty()).collect(Collectors.toList());
                                            StringBuilder stringBuilder = new StringBuilder();
                                            if (punishments.size() == 1) {
                                                stringBuilder.append("`").append(punishments.get(0)).append("`");
                                            } else {
                                                for (String str : punishments) {
                                                    stringBuilder.append("`").append(str).append("`").append(", ");
                                                }
                                            }
                                            messageIds.forEach(id -> bot.getMongoRequestRegistry().addPunishmentsToPunishment(id, punishments));
                                            messageIds.forEach(id -> {
                                                final String[] message = {stringBuilder.toString()};
                                                channel.getMessageById(id).queue(editMsg -> {
                                                    if (punishments.size() > 1)
                                                        message[0] = stringBuilder.toString().substring(0, 2);
                                                    punishmentEmbedBuilder = punishmentEmbedBuilder.appendDescription("\n**Punishments:** " + message[0]);
                                                    executorService.submit(() -> editMsg.editMessage(punishmentEmbedBuilder.build()).queue());
                                                });
                                            });
                                        } else {

                                        }
                                        if (!punishmentError[0])
                                            reasonStep(bot, success, channel);
                                    } else {
                                        punishmentError[0] = true;
                                        this.run();
                                    }
                                });
                                hasClickedPunishments[0] = true;
                            }
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void reasonStep(ShadowBotStaff bot, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<Message> reasonHistory = new ArrayList<>();
                ReactionMenu reasonPm = new ReactionMenu.Builder(bot.getJda())
                        .setEmbed(new EmbedBuilder().setTitle("Punishment Creation: Step Four").setColor(Color.decode("#04E11E")).setDescription("Please enter the reason for the punishment!").setFooter("Progress: 80%", null).build())
                        .onClick("\u2705", (done, server1) -> {
                            if (!hasClickedInfo[0]) {
                                success.getHistory().retrievePast(20).queue((history) -> {
                                    for (Message messages : history) {
                                        if (messages.getAuthor().isBot())
                                            break;
                                        reasonHistory.add(messages);
                                    }
                                    if (!reasonHistory.isEmpty()) {
                                        if (reasonHistory.size() == 1) {
                                            String msg = reasonHistory.get(0).getContentRaw();
                                            String[] split = msg.split(", ");
                                            List<String> reasons = Arrays.stream(split).filter(s -> !s.isEmpty()).collect(Collectors.toList());
                                            StringBuilder stringBuilder = new StringBuilder();
                                            if (reasons.size() == 1) {
                                                stringBuilder.append("`").append(reasons.get(0)).append("`");
                                            } else {
                                                for (String str : reasons) {
                                                    stringBuilder.append("`").append(str).append("`").append(", ");
                                                }
                                            }
                                            messageIds.forEach(id -> bot.getMongoRequestRegistry().addReasonToPunishment(id, reasons));
                                            messageIds.forEach(id -> {
                                                final String[] message = {stringBuilder.toString()};
                                                channel.getMessageById(id).queue(editMsg -> {
                                                    if (reasons.size() > 1)
                                                        message[0] = stringBuilder.toString().substring(0, 2);
                                                    punishmentEmbedBuilder = punishmentEmbedBuilder.appendDescription("\n**Reasons:** " + message[0]);
                                                    executorService.submit(() -> editMsg.editMessage(punishmentEmbedBuilder.build()).queue());
                                                });
                                            });
                                        } else {

                                        }
                                        addEvidence(bot, success, channel);
                                    } else {
                                        this.run();
                                    }
                                });
                                hasClickedInfo[0] = true;
                            }
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void addEvidence(ShadowBotStaff bot, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<Message> reasonHistory = new ArrayList<>();
                ReactionMenu reasonPm = new ReactionMenu.Builder(bot.getJda())
                        .setEmbed((evidenceError[0] ? new EmbedBuilder().setTitle("Error").setColor(Color.RED).setDescription("You didn't give any evidence! \n Please upload or give links to images of evidence! \n The first image you upload or link will be the image of the embed!").setFooter("Progress: 90%", null).build()
                                : new EmbedBuilder().setTitle("Punishment Creation: Step Five").setColor(Color.decode("#04E11E")).setDescription("Please upload or give links to images of evidence! \n The first image you upload or link will be the image of the embed!").setFooter("Progress: 90%", null).build()))
                        .onClick("\u2705", (done, server1) -> {
                            if (!hasClickedEvidence[0]) {
                                success.getHistory().retrievePast(20).queue((history) -> {
                                    for (Message messages : history) {
                                        if (messages.getAuthor().isBot())
                                            break;
                                        reasonHistory.add(messages);
                                    }
                                    if (!reasonHistory.isEmpty()) {
                                        if (reasonHistory.size() == 1) {
                                            String msg = reasonHistory.get(0).getContentRaw();
                                            String[] split = msg.split(", ");
                                            List<String> images = Arrays.stream(split).filter(s -> !s.isEmpty()).collect(Collectors.toList());
                                            StringBuilder stringBuilder = new StringBuilder();
                                            if (images.size() == 1) {
                                                punishmentEmbedBuilder = punishmentEmbedBuilder.setImage(images.get(0));
                                            } else {
                                                boolean firstImage = false;
                                                int count = 1;
                                                for (String str : images) {
                                                    if (!firstImage) {
                                                        punishmentEmbedBuilder = punishmentEmbedBuilder.setImage(images.get(0));
                                                        firstImage = true;
                                                    } else {
                                                        stringBuilder.append("[" + "Evidence").append(count).append("](").append(str).append(")").append(", ");
                                                        count++;
                                                    }
                                                }
                                            }
                                            messageIds.forEach(id -> bot.getMongoRequestRegistry().addImagesToPunishment(id, images));
                                            messageIds.forEach(id -> {
                                                final String[] imageLinks = {stringBuilder.toString()};
                                                channel.getMessageById(id).queue(editMsg -> {
                                                    if (images.size() > 1)
                                                        imageLinks[0] = stringBuilder.toString().substring(0, 2);
                                                    punishmentEmbedBuilder = punishmentEmbedBuilder.appendDescription("\n**Evidence:** " + imageLinks[0]);
                                                    executorService.submit(() -> editMsg.editMessage(punishmentEmbedBuilder.build()).queue());
                                                });
                                            });
                                        } else {

                                        }
                                        if (!evidenceError[0])
                                            addAdditionalInfo(bot, success, channel);
                                    } else {
                                        evidenceError[0] = true;
                                        executorService.submit(this);
                                        //this.run();
                                    }
                                });
                                hasClickedEvidence[0] = true;
                            }
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void addAdditionalInfo(ShadowBotStaff bot, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<Message> infoHistory = new ArrayList<>();
                ReactionMenu infoPm = new ReactionMenu.Builder(bot.getJda())
                        .setEmbed(new EmbedBuilder().setTitle("Punishment Creation: Optional Step").setColor(Color.decode("#04E11E")).setDescription("If desired, please enter the additional info for the punishment!").setFooter("Progress: 100%", null).build())
                        .onClick("\u2705", (done, server1) -> {
                            if (!hasClickedInfo[0]) {
                                success.getHistory().retrievePast(20).queue((history) -> {
                                    for (Message messages : history) {
                                        if (messages.getAuthor().isBot())
                                            break;
                                        infoHistory.add(messages);
                                    }
                                    if (!infoHistory.isEmpty()) {
                                        if (infoHistory.size() == 1) {
                                            String msg = infoHistory.get(0).getContentRaw();
                                            String[] split = msg.split(", ");
                                            List<String> reasons = Arrays.stream(split).filter(s -> !s.isEmpty()).collect(Collectors.toList());
                                            StringBuilder stringBuilder = new StringBuilder();
                                            if (reasons.size() == 1) {
                                                stringBuilder.append("`").append(reasons.get(0)).append("`");
                                            } else {
                                                for (String str : reasons) {
                                                    stringBuilder.append("`").append(str).append("`").append(", ");
                                                }
                                            }
                                            messageIds.forEach(id -> bot.getMongoRequestRegistry().addReasonToPunishment(id, reasons));
                                            messageIds.forEach(id -> {
                                                final String[] message = {stringBuilder.toString()};
                                                channel.getMessageById(id).queue(editMsg -> {
                                                    if (reasons.size() > 1)
                                                        message[0] = stringBuilder.toString().substring(0, stringBuilder.toString().length() - 2);
                                                    punishmentEmbedBuilder = punishmentEmbedBuilder.appendDescription("\n**Reasons:** " + message[0]);
                                                    executorService.submit(() -> editMsg.editMessage(punishmentEmbedBuilder.build()).queue());
                                                });

                                            });
                                        } else {

                                        }
                                        //if (!reasonError[0])
                                        //reasonStep(bot, success, channel);
                                    }
                                });
                                hasClickedInfo[0] = true;
                            }
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }
}
