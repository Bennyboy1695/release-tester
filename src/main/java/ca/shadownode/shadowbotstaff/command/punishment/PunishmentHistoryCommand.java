package ca.shadownode.shadowbotstaff.command.punishment;

import ca.shadownode.shadowbotstaff.ShadowBotStaff;
import ca.shadownode.shadowbotstaff.logging.Logger;
import ca.shadownode.shadowbotstaff.pagination.Page;
import ca.shadownode.shadowbotstaff.pagination.PageBuilder;
import ca.shadownode.shadowbotstaff.pagination.PaginationEmbed;
import ca.shadownode.shadowbotstaff.utilities.Util;
import com.mongodb.BasicDBObject;
import me.bhop.bjdautilities.command.annotation.Command;
import me.bhop.bjdautilities.command.annotation.Execute;
import me.bhop.bjdautilities.command.result.CommandResult;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.utils.tuple.Pair;
import org.bson.Document;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Stream;

@Command(label = "history", description = "Will return punishments matching the query you enter!", usage = "history [-u:Otisnfur -s:global -a:@Bennyboy1695]")
public class PunishmentHistoryCommand {

    @Execute
    public CommandResult onHistory(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowBotStaff bot) {
        bot.getMessenger().sendEmbed(channel, new EmbedBuilder().setTitle("Error").setColor(Color.RED).setDescription("History command is currently not usable!").build(), 10);
        args = new ArrayList<>();
        if (!args.isEmpty()) {
            int page = 1;
            ArrayList<String> arguments = new ArrayList<>();
            Map<Character, String> flags = new HashMap<>();

            for (String arg : args) {
                if (arg.length() == 2 && arg.charAt(0) == '-')
                    flags.put(arg.charAt(1), null);
                else if (arg.length() > 3 && arg.charAt(0) == '-' && arg.charAt(2) == ':')
                    flags.put(arg.charAt(1), arg.substring(3));
                else
                    arguments.add(arg);
            }

            BasicDBObject queries = new BasicDBObject();
            for (Map.Entry<Character, String> flag : flags.entrySet()) {
                if (flag.getKey().equals('u')) {
                    Pair pair = Util.getPlayerInfoFromUsername(flag.getValue());
                    queries.append("UUID", pair.getLeft().toString());
                }
                if (flag.getKey().equals('s')) {
                    List<String> server = new ArrayList<>();
                    if (flag.getValue().equalsIgnoreCase("global")) {
                        server.add("Global");
                    } else {
                        server.add(flag.getValue());
                    }
                    queries.append("servers", server);
                }
                if (flag.getKey().equals('a')) {
                    long userId = message.getMentionedMembers().get(0).getUser().getIdLong();
                    List<String> staff = Arrays.asList(String.valueOf(userId));
                    queries.append("staff", staff);
                }
            }

            if (!queries.isEmpty()) {
                ArrayList<Document> punishments = bot.getMongoRequestRegistry().getPunishmentsBasedOnQuery(queries);
                if (!punishments.isEmpty()) {
                    int maxPages = punishments.size() % 3 == 0 ? punishments.size() / 3 : punishments.size() / 3 + 1;
                    int count = 1;
                    List<Page> content = new ArrayList<>();
                    PaginationEmbed.Builder builder = new PaginationEmbed.Builder(bot.getJda());
                    while (count <= maxPages) {
                        content.add(generatePage(page, maxPages, punishments.stream().skip((count - 1) * 3).limit(3), member, channel, bot));
                        count++;
                    }
                    content.forEach(builder::addPage);
                    if (page < maxPages)
                        page++;
                    count++;
                    builder.buildAndDisplay(channel);
                    return CommandResult.success();
                }
            } else {
                bot.getMessenger().sendEmbed(channel, new EmbedBuilder().setColor(Color.RED).setTitle("Error").setDescription("We were unable to find any results matching your query!").build(), 15);
                return CommandResult.success();
            }
        }
        return CommandResult.success();
    }

    private Page generatePage(int page, int maxPages, Stream<Document> punishments, Member sender, TextChannel channel, ShadowBotStaff bot) {
        PageBuilder pageBuilder = new PageBuilder().setEntryLimit(3).includeTimestamp(true).setColor(Color.CYAN).setTitle("**Punishments Matching Query**");
        pageBuilder.setFooter("Page " + page + " of " + maxPages, sender.getJDA().getSelfUser().getAvatarUrl());
        punishments.forEach(document -> {
            Pair pair = Util.getPlayerInfoFromUUID(UUID.fromString(document.get("UUID").toString()));
            List<String> staff = (List<String>) document.get("staff");
            StringBuilder staffAttended = new StringBuilder();
            for (String str : staff) {
                Member member = channel.getGuild().getMemberById(str);
                staffAttended.append(member.getAsMention()).append(", ");
            }
            List<String> serversStrings = (List<String>) document.get("servers");
            StringBuilder servers = new StringBuilder();
            for (String str : serversStrings) {
                servers.append(str).append(", ");
            }
            List<String> punishStrings = (List<String>) document.get("punishments");
            StringBuilder punish = new StringBuilder();
            for (String str : punishStrings) {
                punish.append(str).append(", ");
            }
            Message message = null;
            try {
                message = channel.getMessageById(document.get("_id").toString()).complete();
            } catch (Exception e) {
                Logger.error("Message was not found!");
                e.printStackTrace();
            }
            pageBuilder.addContent(false, "**" + pair.getRight().toString() + "** Punishment Preview",
                    (staffAttended.toString().isEmpty() ? "\u0020\u2022\u0020**Staff:** " + "Not Set" : "\u0020\u2022\u0020**Staff:** " + staffAttended.toString().substring(0, staffAttended.toString().length() - 2)),
                    (servers.toString().isEmpty() ? "\u0020\u2022\u0020**Servers:** " + "Not Set" : "\u0020\u2022\u0020**Servers:** " + servers.toString().substring(0, servers.toString().length() - 2)),
                    (punish.toString().isEmpty() ? "\u0020\u2022\u0020**Punishments:** " + "Not Set" : "\u0020\u2022\u0020**Punishments:** " + punish.toString().substring(0, punish.toString().length() - 2)),
                    "\u0020\u2022\u0020**Jump Url:** ", (message != null ? message.getJumpUrl() : "Invalid messageID was found!"));
        });
        return pageBuilder.build();
    }
}
