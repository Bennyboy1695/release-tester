package ca.shadownode.shadowbotstaff.storage;

import ca.shadownode.shadowbotstaff.ShadowBotStaff;
import ca.shadownode.shadowbotstaff.utilities.Punishment;
import com.google.gson.JsonObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

public class SqlManager {

    private Database database;

    public SqlManager(String hostname, int port, String databaseName, String username, String password) {
        try {
            database = new MySQL(hostname, port, databaseName, username, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            writeTables();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void writeTables() throws SQLException, ClassNotFoundException {
        String punishmentsTable = "CREATE TABLE IF NOT EXISTS `sbs_punishments` (" +
                "`messageId`      VARCHAR(18)        PRIMARY KEY NOT NULL," +
                "`uuid`   VARCHAR(36)         NOT NULL," +
                "`servers`  VARCHAR(128)         NOT NULL," +
                "`punishments`    VARCHAR(128)         NOT NULL," +
                "`reasons`   VARCHAR(512)       NOT NULL," +
                "`info`    VARCHAR(1000)         ," +
                "`images`    VARCHAR(1000)         " +
                ");";

        database.openConnection();
        database.updateSQL(punishmentsTable);
        database.closeConnection();
    }

    public boolean addNewPunishment(long messageId, UUID uuid, String servers, String punishments, String reasons) {
        try (Connection c = database.openConnection()) {
            String statement = "INSERT INTO sbs_punishments (messageId,uuid,servers,punishments,reasons) VALUES(?,?,?,?,?);";
            try (PreparedStatement ps = c.prepareStatement(statement)) {
                ps.setLong(1, messageId);
                ps.setString(2, uuid.toString());
                ps.setString(3, servers);
                ps.setString(4, punishments);
                ps.setString(5, reasons);
                ps.executeUpdate();
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean addImagesToPunishment(long messageId, String images) {
        return false;
    }

    public boolean addInfoToPunishment(long messageId, String info) {
        return false;
    }

    public static class PunishPluginSqlManager {

        private Database database;
        private ShadowBotStaff bot;

        public PunishPluginSqlManager(ShadowBotStaff bot, String hostname, int port, String databaseName, String username, String password) {
            this.bot = bot;
            try {
                database = new MySQL(hostname, port, databaseName, username, password);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public UUID getPlayerUUIDFromId(int id) {
            try (Connection c = database.openConnection()) {
                String statement = "SELECT * FROM player_data WHERE player_id='" + id + "';";
                try (PreparedStatement ps = c.prepareStatement(statement)) {
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next())
                            return UUID.fromString(rs.getString("uuid"));
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }

        public String getServerNameFromId(int id) {
            try (Connection c = database.openConnection()) {
                String statement = "SELECT * FROM server_data WHERE server_id='" + id + "';";
                try (PreparedStatement ps = c.prepareStatement(statement)) {
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next())
                            return rs.getString("server_name");
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }

        public ArrayList<Punishment> getBans() {
            ArrayList<Punishment> punishments = new ArrayList<>();
            long last = 0L;
            try (Connection c = database.openConnection()) {
                String statement = "SELECT * FROM player_bans;";
                try (PreparedStatement ps = c.prepareStatement(statement)) {
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            long time = rs.getTimestamp("banned").getTime();
                            if (time > bot.getMainConfig().getConfigValue("NO_TOUCH", "lastBan").getAsLong())
                                punishments.add(new Punishment(Punishment.PunishmentType.BAN,
                                        rs.getInt("banned_player"),
                                        time,
                                        (rs.getTimestamp("banned_until") != null ? rs.getTimestamp("banned_until").getTime() : 0L),
                                        rs.getInt("banned_by"),
                                        rs.getInt("from_server"),
                                        (rs.getTimestamp("banned_until") == null),
                                        rs.getString("reason")));
                            last = time;
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            updateLastBan(last);
            return punishments;
        }

        public ArrayList<Punishment> getMutes() {
            ArrayList<Punishment> punishments = new ArrayList<>();
            long last = 0L;
            try (Connection c = database.openConnection()) {
                String statement = "SELECT * FROM player_mutes;";
                try (PreparedStatement ps = c.prepareStatement(statement)) {
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            if (rs.getTimestamp("muted").getTime() > bot.getMainConfig().getConfigValue("NO_TOUCH", "lastMute").getAsLong())
                                punishments.add(new Punishment(Punishment.PunishmentType.MUTE,
                                        rs.getInt("muted_player"),
                                        rs.getTimestamp("muted").getTime(),
                                        (rs.getTimestamp("muted_until") != null ? rs.getTimestamp("muted_until").getTime() : 0L),
                                        rs.getInt("muted_by"),
                                        rs.getInt("from_server"),
                                        (rs.getTimestamp("muted_until") == null),
                                        rs.getString("reason")));
                            last = rs.getTimestamp("muted").getTime();
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            updateLastMute(last);
            return punishments;
        }

        private void updateLastBan(long newLast) {
            JsonObject config = bot.getMainConf();
            JsonObject object = config.get("NO_TOUCH").getAsJsonObject();
            if (object.get("lastBan").getAsLong() < newLast) {
                object.addProperty("lastBan", newLast);
            }
            bot.getMainConfig().saveConfig("config", config);
            bot.reloadConfig();
        }

        private void updateLastMute(long newLast) {
            JsonObject config = bot.getMainConf();
            JsonObject object = config.get("NO_TOUCH").getAsJsonObject();
            if (object.get("lastMute").getAsLong() < newLast) {
                object.addProperty("lastMute", newLast);
            }
            bot.getMainConfig().saveConfig("config", config);
            bot.reloadConfig();
        }
    }

}
