package ca.shadownode.shadowbotstaff.storage.oldMongo;

import ca.shadownode.shadowbotstaff.ShadowBotStaff;
import com.mongodb.BasicDBObject;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class MongoRequestRegistry {
    private final MongoController controller;
    private ShadowBotStaff bot;

    public MongoRequestRegistry(ShadowBotStaff bot) {
        this.controller = bot.getMongoController();
        this.bot = bot;
    }

    public void createNewPunishment(String messageId, String playerUUID) {
        Document document = new Document();
        document.put("_id", messageId);
        document.put("UUID", playerUUID);
        document.put("staff", new ArrayList<>());
        document.put("servers", new ArrayList<>());
        document.put("punishments", new ArrayList<>());
        document.put("reasons", new ArrayList<>());
        document.put("infos", new ArrayList<>());
        document.put("images", new ArrayList<>());
        controller.getCollection(bot.getMainConfig().getConfigValue("mongo_settings", "punishment_collection").getAsString()).insertOne(document);
    }

    public void addStaffToPunishment(String messageId, List<String> staffUserIds) {
        Document filter = new Document("_id", messageId);
        Document newStuff = new Document("staff", staffUserIds);
        Document updateOp = new Document("$set", newStuff);
        controller.getCollection(bot.getMainConfig().getConfigValue("mongo_settings", "punishment_collection").getAsString()).updateOne(filter, updateOp);
    }

    public void addServersToPunishment(String messageId, List<String> staffUserIds) {
        Document filter = new Document("_id", messageId);
        Document newStuff = new Document("servers", staffUserIds);
        Document updateOp = new Document("$set", newStuff);
        controller.getCollection(bot.getMainConfig().getConfigValue("mongo_settings", "punishment_collection").getAsString()).updateOne(filter, updateOp);
    }

    public void addPunishmentsToPunishment(String messageId, List<String> punishments) {
        Document filter = new Document("_id", messageId);
        Document newStuff = new Document("punishments", punishments);
        Document updateOp = new Document("$set", newStuff);
        controller.getCollection(bot.getMainConfig().getConfigValue("mongo_settings", "punishment_collection").getAsString()).updateOne(filter, updateOp);
    }

    public void addReasonToPunishment(String messageId, List<String> reasons) {
        Document filter = new Document("_id", messageId);
        Document newStuff = new Document("reasons", reasons);
        Document updateOp = new Document("$set", newStuff);
        controller.getCollection(bot.getMainConfig().getConfigValue("mongo_settings", "punishment_collection").getAsString()).updateOne(filter, updateOp);
    }

    public void addAdditionalInfoToPunishment(String messageId, List<String> infos) {
        Document filter = new Document("_id", messageId);
        Document newStuff = new Document("infos", infos);
        Document updateOp = new Document("$set", newStuff);
        controller.getCollection(bot.getMainConfig().getConfigValue("mongo_settings", "punishment_collection").getAsString()).updateOne(filter, updateOp);
    }

    public void addImagesToPunishment(String messageId, List<String> images) {
        Document filter = new Document("_id", messageId);
        Document newStuff = new Document("images", images);
        Document updateOp = new Document("$set", newStuff);
        controller.getCollection(bot.getMainConfig().getConfigValue("mongo_settings", "punishment_collection").getAsString()).updateOne(filter, updateOp);
    }

    public ArrayList<Document> getPunishmentsBasedOnQuery(BasicDBObject query) {
        ArrayList<Document> found = new ArrayList<>();
        for (Document document : controller.getCollection(bot.getMainConfig().getConfigValue("mongo_settings", "punishment_collection").getAsString()).find(query)) {
            found.add(document);
        }
        return found;
    }
}
