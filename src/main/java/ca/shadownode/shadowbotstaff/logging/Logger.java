package ca.shadownode.shadowbotstaff.logging;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

public class Logger {

    private static final String FORMAT = "[%s] [Bot] [%s]: %s";

    public static void info(String msg) {
        System.out.println(String.format(FORMAT, OffsetDateTime.now().format(DateTimeFormatter.ofPattern("MM/dd HH:mm:ss")), "INFO", msg));
    }

    public static void error(String msg) {
        System.out.println(LogColour.RED + String.format(FORMAT, OffsetDateTime.now().format(DateTimeFormatter.ofPattern("MM/dd HH:mm:ss")), "ERROR", msg));
    }

    public static void warn(String msg) {
        System.out.println(LogColour.YELLOW + String.format(FORMAT, OffsetDateTime.now().format(DateTimeFormatter.ofPattern("MM/dd HH:mm:ss")), "WARN", msg));
    }

    public static void debug(String msg) {
        System.out.println(LogColour.CYAN + String.format(FORMAT, OffsetDateTime.now().format(DateTimeFormatter.ofPattern("MM/dd HH:mm:ss")), "DEBUG", msg));
    }

}
