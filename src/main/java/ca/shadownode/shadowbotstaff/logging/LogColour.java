package ca.shadownode.shadowbotstaff.logging;

public enum LogColour {
    RESET("\u001B[0m"),

    WHITE("\u001B[37m"),
    BLACK("\u001B[30m"),
    RED("\u001B[31m"),
    YELLOW("\u001B[33m"),
    GREEN("\u001B[32m"),
    BLUE("\u001B[34m"),
    CYAN("\u001B[36m"),
    PURPLE("\u001B[35m"),

    WHITE_BG("\u001B[47m"),
    BLACK_BG("\u001B[40m"),
    RED_BG("\u001B[41m"),
    YELLOW_BG("\u001B[43m"),
    GREEN_BG("\u001B[42m"),
    BLUE_BG("\u001B[44m"),
    CYAN_BG("\u001B[46m"),
    PURPLE_BG("\u001B[45m");

    private String code;
    LogColour(String code) {
        this.code = code;
    }
    public static LogColour of(int ordinal) {
        for (LogColour colour: values())
            if (colour.ordinal() == ordinal)
                return colour;
        return RESET;
    }
    @Override
    public String toString() {
        return code;
    }
}
