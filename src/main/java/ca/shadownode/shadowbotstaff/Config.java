package ca.shadownode.shadowbotstaff;

import com.google.gson.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Map;

public class Config {

    private ShadowBotStaff bot;
    private Path configDirectory;
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private JsonObject conf;

    Config(ShadowBotStaff bot, Path configDirectory) {
        this.bot = bot;
        this.configDirectory = configDirectory;
    }

    JsonObject newConfig(String configName, JsonObject configOptions) {
        JsonObject conf = null;
        Path config = configDirectory.resolve(configName + ".json");
        try {
            if (!Files.exists(config)) {
                Files.createFile(config);
                write(config, configOptions);
            }
            if (hasAllEntries(read(config).getAsJsonObject(), configOptions)) {
                conf = read(config).getAsJsonObject();
            } else {
                write(config, writeNotFoundDefaults(read(config).getAsJsonObject(), configOptions));
                conf = read(config).getAsJsonObject();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.conf = conf;
        return conf;
    }

    private JsonElement read(Path config) {
        JsonElement element = null;
        try (BufferedReader reader = Files.newBufferedReader(config)) {
            JsonParser parser = new JsonParser();
            element = parser.parse(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return element;
    }

    private void write(Path config, JsonObject object) {
        try (BufferedWriter writer = Files.newBufferedWriter(config)) {
            writer.write(gson.toJson(object));
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveConfig(String configName, JsonElement conf) {
        try (BufferedWriter writer = Files.newBufferedWriter(configDirectory.resolve(configName + ".json"))) {
            writer.write(gson.toJson(conf));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static JsonObject mainConfigDefaults() {
        JsonObject main = new JsonObject();
        main.addProperty("apikey", "change_me");
        main.addProperty("apilink", "https://pterodactyl.app");
        main.addProperty("prefix", "?");
        main.addProperty("token", "change_me");
        main.addProperty("statusRoleId", 0L);
        main.addProperty("punishmentChannelId", 0L);
        main.addProperty("punishment_update_check", 60L);
        JsonArray blackListed = new JsonArray();
        blackListed.add("ShadowBotStaff");
        main.add("blacklistedServers", blackListed);
        JsonObject sqlSettings = new JsonObject();
        sqlSettings.addProperty("hostname", "change_me");
        sqlSettings.addProperty("port", 33333);
        sqlSettings.addProperty("database", "change_me");
        sqlSettings.addProperty("username", "change_me");
        sqlSettings.addProperty("password", "change_me");
        JsonObject banPluginSettings = new JsonObject();
        banPluginSettings.addProperty("hostname", "change_me");
        banPluginSettings.addProperty("port", 3306);
        banPluginSettings.addProperty("database", "change_me");
        banPluginSettings.addProperty("username", "change_me");
        banPluginSettings.addProperty("password", "change_me");
        banPluginSettings.addProperty("checkBans", true);
        banPluginSettings.addProperty("checkMutes", true);
        JsonObject mongoSettings = new JsonObject();
        mongoSettings.addProperty("hostname", "change_me");
        mongoSettings.addProperty("port", 3306);
        mongoSettings.addProperty("database", "change_me");
        mongoSettings.addProperty("username", "change_me");
        mongoSettings.addProperty("password", "change_me");
        mongoSettings.addProperty("authDb", "change_me");
        mongoSettings.addProperty("punishment_collection", "punishments");
        //main.add("mongo_settings", mongoSettings);
        main.add("sql_settings", sqlSettings);
        main.add("hammerSqlSettings", banPluginSettings);
        JsonObject noTouch = new JsonObject();
        noTouch.addProperty("lastMute", Instant.now().toEpochMilli());
        noTouch.addProperty("lastBan", Instant.now().toEpochMilli());
        noTouch.addProperty("inlockdown", false);
        main.add("NO_TOUCH", noTouch);
        return main;
    }

    public JsonObject writeNotFoundDefaults(JsonObject config, JsonObject configOptions) {
        JsonObject finished = new JsonObject();
        for (Map.Entry<String, JsonElement> entrySet : configOptions.entrySet()) {
            if (!config.has(entrySet.getKey())) {
                finished.add(entrySet.getKey(), entrySet.getValue());
            } else {
                finished.add(entrySet.getKey(), config.get(entrySet.getKey()));
            }
        }
        return finished;
    }

    public boolean hasAllEntries(JsonObject config, JsonObject configOptions) {
        int count = 0;
        for (Map.Entry<String, JsonElement> entrySet : configOptions.entrySet()) {
            if (config.has(entrySet.getKey())) {
                count++;
            }
        }
        return (count == configOptions.size());
    }

    public <T extends JsonElement> T getConfigValue(String... keys) {
        JsonObject parent = (JsonObject) conf;
        JsonElement temp = parent.get(keys[0]);
        if (temp.isJsonArray())
            return (T) temp.getAsJsonArray();
        if (temp.isJsonPrimitive())
            return (T) temp.getAsJsonPrimitive();
        JsonObject object = temp.getAsJsonObject();
        try {
            for (int i = 1; i < keys.length; i++) {
                temp = object.get(keys[i]);
                if (temp.isJsonArray())
                    return (T) temp.getAsJsonArray();
                if (temp.isJsonPrimitive())
                    return (T) temp.getAsJsonPrimitive();
                object = temp.getAsJsonObject();
            }
        } catch (NullPointerException e) {
            return (T) object;
        }
        return (T) object;
    }
}
