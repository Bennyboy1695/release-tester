package ca.shadownode.shadowbotstaff.utilities;

public class Server {

    private String name, identifier;

    public Server(String name, String identifier) {
        this.name = name;
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public String getIdentifier() {
        return identifier;
    }
}
