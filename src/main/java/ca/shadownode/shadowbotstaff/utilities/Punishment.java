package ca.shadownode.shadowbotstaff.utilities;

public class Punishment {

    private PunishmentType type;
    private int player, staff, server;
    private long punishedOn, punishedUntil;
    private String reason;
    private boolean isPerm;

    public Punishment(PunishmentType type, int player, long punishedOn, long punishedUntil, int staff, int server, boolean isPerm, String reason) {
        this.type = type;
        this.player = player;
        this.punishedOn = punishedOn;
        this.punishedUntil = punishedUntil;
        this.staff = staff;
        this.server = server;
        this.isPerm = isPerm;
        this.reason = reason;
    }

    public PunishmentType getType() {
        return type;
    }

    public int getPlayer() {
        return player;
    }

    public long getPunishedOn() {
        return punishedOn;
    }

    public long getPunishedUntil() {
        return punishedUntil;
    }

    public int getStaff() {
        return staff;
    }

    public int getServer() {
        return server;
    }

    public boolean isPerm() {
        return isPerm;
    }

    public String getReason() {
        return reason;
    }

    public enum PunishmentType {
        BAN,
        MUTE;
    }
}
