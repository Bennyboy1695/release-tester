package ca.shadownode.shadowbotstaff.utilities;

import ca.shadownode.shadowbotstaff.ShadowBotStaff;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.dv8tion.jda.core.utils.tuple.MutablePair;
import net.dv8tion.jda.core.utils.tuple.Pair;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {

    public static ArrayList<String> getBlacklistedServers(ShadowBotStaff bot) {
        ArrayList<String> blacklisted = new ArrayList<>();
        for (Object object : bot.getMainConfig().getConfigValue("blacklistedServers").getAsJsonArray()) {
            blacklisted.add(String.valueOf(object));
        }
        return  blacklisted;
    }

    public static String getClosestMatchServer(ShadowBotStaff bot, String server) {
        ArrayList<String> matchedServers = new ArrayList<>();
        int lowest = 100;
        for (Server server1 : getAllServers(bot)) {
            int maybe = Util.computeLevenshteinDistance(server, server1.getName());
            if (maybe < lowest) {
                lowest = maybe;
                matchedServers = new ArrayList<>();
                matchedServers.add(String.valueOf(server1.getName()));
            } else if (maybe == lowest) {
                matchedServers.add(String.valueOf(server1.getName()));
            }
        }
        return matchedServers.get(0);
    }

    public static ArrayList<Server> getAllServers(ShadowBotStaff bot) {
        ArrayList<Server> servers = new ArrayList<>();
        try {
            URL url = new URL(bot.getMainConfig().getConfigValue("apilink").getAsString() + "/api/client");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Bearer " + bot.getMainConfig().getConfigValue("apikey").getAsString());
            connection.setRequestProperty("Accept", "Application/vnd.pterodactyl.v1+json");
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            JsonParser parser = new JsonParser();
            JsonArray array = parser.parse(content.toString()).getAsJsonObject().get("data").getAsJsonArray();
            for (Object object : array) {
                JsonObject attributes = ((JsonObject) object).get("attributes").getAsJsonObject();
                if (!getBlacklistedServers(bot).contains(attributes.get("name").getAsString()))
                    servers.add(new Server(attributes.get("name").getAsString().replace("ShadowNode ", ""), attributes.get("identifier").getAsString()));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return servers;
    }

    public static ServerInfo getServerInfo(ShadowBotStaff bot, String id) {
        boolean online = false;
        double cpu = 0.0;
        String memory = "0/0", disk = "0/0";
        for (Server server : bot.getAvailableServers()) {
            if (server.getIdentifier().equals(id)) {
                JsonObject object = getInfoForServer(bot, id);
                JsonObject obj =  object.get("attributes").getAsJsonObject();
                    online = (obj.get("state").getAsString().equalsIgnoreCase("on"));
                    if (online) {
                        memory = obj.get("memory").getAsJsonObject().get("current").getAsInt() + "/" + obj.get("memory").getAsJsonObject().get("limit").getAsInt();
                        disk = obj.get("disk").getAsJsonObject().get("current").getAsInt() + "/" + obj.get("disk").getAsJsonObject().get("limit").getAsInt();
                        cpu = obj.get("cpu").getAsJsonObject().get("current").getAsDouble();
                    }
            }
        }
        return new ServerInfo(online, memory, cpu, disk);
    }

    public static JsonObject getInfoForServer(ShadowBotStaff bot, String id) {
        JsonObject object = new JsonObject();
        try {
            URL url = new URL(bot.getMainConfig().getConfigValue("apilink").getAsString() + "/api/client/servers/" + id + "/utilization");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Bearer " + bot.getMainConfig().getConfigValue("apikey").getAsString());
            connection.setRequestProperty("Accept", "Application/vnd.pterodactyl.v1+json");
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            JsonParser parser = new JsonParser();
            object = parser.parse(content.toString()).getAsJsonObject();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return object;
    }

    public static String convertToDaysHoursMinutes(long minutes) {
        int day = (int)TimeUnit.MINUTES.toDays(minutes);
        long hours = TimeUnit.MINUTES.toHours(minutes) - (day *24);
        long minute = TimeUnit.MINUTES.toMinutes(minutes) - (TimeUnit.MINUTES.toHours(minutes)* 60);
        String result = "";
        if (day != 0){
            result += day;
            result += "d";

            return result;
        }
        if (hours != 0){
            result += hours;
            result += "h";
        }
        if (minute != 0){
            result += minute;
            result += "m";
        }
        return result;
    }

    public static long timeToMillis(long year, long month, long week, long day, long hour, long min, long sec) {
        return (year*31536000000L) + (month*2628000000L) + TimeUnit.DAYS.toMillis(week*7) + TimeUnit.DAYS.toMillis(day) + TimeUnit.HOURS.toMillis(hour) + TimeUnit.MINUTES.toMillis(min) + TimeUnit.SECONDS.toMillis(sec);
    }

    public static long stringToMillisConverter(CharSequence text){
        Pattern pattern = Pattern.compile("([0-9]+w)?([0-9]+d)?([0-9]+h)?([0-9]+m)?([0-9]+s)?", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);
        if (matcher.matches()){
            return timeToMillis(0L,0L,
                    (matcher.group(1) != null ? Long.valueOf(matcher.group(1).replace("w", "")) : 0L),
                    (matcher.group(2) != null ? Long.valueOf(matcher.group(2).replace("d", "")) : 0L),
                    (matcher.group(3) != null ? Long.valueOf(matcher.group(3).replace("h", "")) : 0L),
                    (matcher.group(4) != null ? Long.valueOf(matcher.group(4).replace("m", "")) : 0L),
                    (matcher.group(5) != null ? Long.valueOf(matcher.group(5).replace("s", "")) : 0L));
        }
        return 0L;
    }

    public static int computeLevenshteinDistance(CharSequence lhs, CharSequence rhs) {
        int[][] distance = new int[lhs.length() + 1][rhs.length() + 1];

        for (int i = 0; i <= lhs.length(); i++)
            distance[i][0] = i;
        for (int j = 1; j <= rhs.length(); j++)
            distance[0][j] = j;

        for (int i = 1; i <= lhs.length(); i++)
            for (int j = 1; j <= rhs.length(); j++)
                distance[i][j] = minimum(
                        distance[i - 1][j] + 1,
                        distance[i][j - 1] + 1,
                        distance[i - 1][j - 1] + ((lhs.charAt(i - 1) == rhs.charAt(j - 1)) ? 0 : 1));

        return distance[lhs.length()][rhs.length()];
    }

    private static int minimum(int a, int b, int c) {
        return Math.min(Math.min(a, b), c);
    }

    public static Pair<UUID, String> getPlayerInfoFromUsername(String username) {
        int responseCode = 0;
        try {
            String url = "https://api.minetools.eu/uuid/" + username;
            URL obj = new URL(url);

            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            responseCode = con.getResponseCode();
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            if (responseCode == 204 || responseCode == 400)
                return null;
            JsonParser parser = new JsonParser();
            JsonObject object = parser.parse(response.toString()).getAsJsonObject();
            String regex = "(.{8})(.{4})(.{4})(.{4})(.{12})";
            String formattedUUID = object.get("id").getAsString().replaceAll(regex, "$1-$2-$3-$4-$5");
            return MutablePair.of(UUID.fromString(formattedUUID), object.get("name").getAsString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Pair<UUID, String> getPlayerInfoFromUUID(UUID uuid) {
        int responseCode = 0;
        try {
            String url = "https://api.minetools.eu/uuid/" + uuid.toString().replace("-", "");
            URL obj = new URL(url);

            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            responseCode = con.getResponseCode();
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            if (responseCode == 204 || responseCode == 400)
                return null;
            JsonParser parser = new JsonParser();
            JsonObject object = parser.parse(response.toString()).getAsJsonObject();
            String regex = "(.{8})(.{4})(.{4})(.{4})(.{12})";
            String formattedUUID = object.get("id").getAsString().replaceAll(regex, "$1-$2-$3-$4-$5");
            return MutablePair.of(UUID.fromString(formattedUUID), object.get("name").getAsString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
