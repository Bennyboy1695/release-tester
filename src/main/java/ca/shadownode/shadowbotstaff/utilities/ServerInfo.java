package ca.shadownode.shadowbotstaff.utilities;

public class ServerInfo {

    private Boolean online;
    private Double cpuPercentage;
    private String memoryUsage, diskUsage;

    public ServerInfo(Boolean online, String memoryUsage, Double cpuPercentage, String diskUsage) {
        this.online = online;
        this.cpuPercentage = cpuPercentage;
        this.diskUsage = diskUsage;
        this.memoryUsage = memoryUsage;
    }

    public Boolean getOnline() {
        return online;
    }

    public Double getCpuPercentage() {
        return cpuPercentage;
    }

    public String getDiskUsage() {
        return diskUsage;
    }

    public String getMemoryUsage() {
        return memoryUsage;
    }
}
