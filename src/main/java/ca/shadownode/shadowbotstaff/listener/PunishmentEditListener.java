package ca.shadownode.shadowbotstaff.listener;

import ca.shadownode.shadowbotstaff.ShadowBotStaff;
import ca.shadownode.shadowbotstaff.logging.Logger;
import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;
import me.bhop.bjdautilities.ReactionMenu;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PunishmentEditListener extends ListenerAdapter {

    private final ExecutorService executorService = Executors.newScheduledThreadPool(2);
    private ShadowBotStaff bot;

    public PunishmentEditListener(ShadowBotStaff bot) {
        this.bot = bot;
    }

    @Override
    public void onMessageReactionAdd(MessageReactionAddEvent event) {
        if (event.getUser().isBot())
            return;
        if (event.getChannel().getIdLong() == bot.getMainConfig().getConfigValue("punishmentChannelId").getAsLong()) {
            if (event.getReactionEmote().getName().equals("\uD83D\uDCDD")) {
                Message punishmentMessage = event.getTextChannel().getMessageById(event.getMessageIdLong()).complete();
                ReactionMenu.Builder sectionMenuBuilder = new ReactionMenu.Builder(bot.getJda());
                EmbedBuilder builder = new EmbedBuilder().setColor(Color.CYAN).setTitle("Pick an option!").setDescription("Please pick the Emote matching the section you would like to edit!");
                List<String> types = Arrays.asList("Evidence", "Extra Info");
                char endCharacter = 'a';
                String alias = "regional_indicator_symbol_";
                for (String type: types) {
                    Emoji emoji = EmojiManager.getForAlias(alias + endCharacter);
                    builder.addField(type + ": ", emoji.getUnicode(), false);
                    sectionMenuBuilder.onClick(emoji.getUnicode(), (click,user) -> {
                        if (user == event.getUser()) {
                            click.clearClickListeners();
                            click.getMessage().clearReactions().complete();
                            click.addReaction("\u2705");
                            click.getMessage().setContent(new EmbedBuilder().setColor(Color.CYAN).setTitle("**Editing:** " + type).setDescription("To edit this section just send your message(s) after this and then click the \u2705 to mark it done!").build());
                            sectionMenuBuilder.onClick("\u2705", (done, user1) -> {
                                if (user1 == event.getUser()) {
                                    ArrayList<Message> beforeBotMessages = new ArrayList<>();
                                    done.getMessage().getChannel().getHistory().retrievePast(20).queue(history -> {
                                        StringBuilder extraInfo = new StringBuilder();
                                        String finalExtraInfo = "";
                                        if (!history.isEmpty()) {
                                            if (type.equalsIgnoreCase("Evidence")) {
                                                for (Message messages : history) {
                                                    if (messages.getAuthor().isBot())
                                                        break;
                                                    if (messages.getAuthor() == event.getUser())
                                                        beforeBotMessages.add(messages);
                                                }
                                                for (Message message : beforeBotMessages) {
                                                    if (!message.getContentRaw().isEmpty()) {
                                                        extraInfo.append(message.getContentRaw()).append(", ");
                                                    } else {
                                                        message.getAttachments().forEach(attachment -> {
                                                            extraInfo.append(attachment.getProxyUrl()).append(", ");
                                                        });
                                                    }
                                                }
                                                finalExtraInfo = extraInfo.substring(0, extraInfo.length() - 2);
                                                try {
                                                    bot.getSqlManager().addImagesToPunishment(punishmentMessage.getIdLong(), extraInfo.toString());
                                                    if (!punishmentMessage.getEmbeds().get(0).getDescription().contains("**Evidence:**")) {
                                                        punishmentMessage.editMessage(new EmbedBuilder(punishmentMessage.getEmbeds().get(0)).appendDescription("\n" + "**Evidence:** " + finalExtraInfo).build()).queue();
                                                    } else {
                                                        punishmentMessage.editMessage(new EmbedBuilder(punishmentMessage.getEmbeds().get(0)).setDescription(punishmentMessage.getEmbeds().get(0).getDescription().replaceFirst("\\**Evidence:\\**.*\\\\n", "**Evidence:** " + finalExtraInfo)).build()).queue();
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            } else {
                                                for (Message messages : history) {
                                                    if (messages.getAuthor().isBot())
                                                        break;
                                                    if (messages.getAuthor() == event.getUser())
                                                        beforeBotMessages.add(messages);
                                                }
                                                for (Message message : beforeBotMessages) {
                                                    extraInfo.append(message.getContentRaw()).append(", ");
                                                }
                                                finalExtraInfo = extraInfo.substring(0, extraInfo.length() - 2);
                                                try {
                                                    bot.getSqlManager().addImagesToPunishment(punishmentMessage.getIdLong(), extraInfo.toString());
                                                    if (!punishmentMessage.getEmbeds().get(0).getDescription().contains("**Additional Info:**")) {
                                                        punishmentMessage.editMessage(new EmbedBuilder(punishmentMessage.getEmbeds().get(0)).appendDescription("\n" + "**Additional Info:** " + finalExtraInfo).build()).queue();
                                                    } else {
                                                        punishmentMessage.editMessage(new EmbedBuilder(punishmentMessage.getEmbeds().get(0)).setDescription(punishmentMessage.getEmbeds().get(0).getDescription().replaceFirst("\\**Additional Info:\\**.*\\\\n", "**Additional Info:** " + finalExtraInfo)).build()).queue();
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                            for (Message message : beforeBotMessages) {
                                                executorService.submit(() -> message.getTextChannel().getMessageById(message.getIdLong()).complete().delete().queue());
                                            }
                                        }
                                    });
                                    done.destroyIn(2);
                                }
                            });
                        }
                    });
                    endCharacter++;
                }
                sectionMenuBuilder.setEmbed(builder.build()).buildAndDisplay(event.getChannel());
                //bot.getMessenger().sendEmbed(event.getChannel(), new EmbedBuilder().setTitle("Error").setDescription("This function isn't implemented yet!").setColor(Color.RED).build(), 10);
                event.getReaction().removeReaction(event.getUser()).complete();
            }
        }
    }
}
